package lisp.geometry;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import javax.swing.JPanel;
import java.awt.*;
import java.awt.geom.AffineTransform;
/*
 * Created by  andreasm 01.06.12 18:22
 */
public class PicturePanel extends JPanel
{
    private static final long REPAINT_DELAY = 300L;
    //
    private transient Picture picture;
    //
    public PicturePanel()
    {
        super();
        //
        init();
    }
    //
    private void init()
    {
        setDoubleBuffered(true);
        setBackground(Color.WHITE);
        setForeground(Color.BLACK);
    }
    //
    public final void setPicture(Picture picture)
    {
        this.picture = picture;
        //
        repaint(REPAINT_DELAY);
    }
    //
    @Override
    protected final void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);
        //
        Picture localpicture = picture;
        //
        if (localpicture == null) return;
        //
        Graphics2D graphics2 = (Graphics2D) graphics;
        graphics2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Dimension size = getSize();
        AffineTransform transform = createTransform(localpicture, size);
        localpicture.paint(graphics2, transform);
    }
    //
    private static AffineTransform createTransform(Picture source, Dimension destination)
    {
        double sourcewidth = source.getWidth();
        double sourceheight = source.getHeight();
        double destinationwidth = destination.getWidth();
        double destinationheight = destination.getHeight();
        double ratio1 = (destinationwidth - 20d) / sourcewidth;
        double ratio2 = (destinationheight - 20d) / sourceheight;
        //
        if (ratio1 > ratio2) ratio1 = ratio2;
        //
        double leftborder = (destinationwidth - sourcewidth * ratio1) / 2d;
        double topborder = (destinationheight - sourceheight * ratio1) / 2d;
        //
        return new AffineTransform(ratio1, 0d, 0d, -ratio1, leftborder, destinationheight - topborder);
    }
}