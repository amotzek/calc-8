/*
 * Copyright (C) 2005, 2014, 2018 Andreas Motzek andreas-motzek@t-online.de
 * 
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.geometry.combinator;
/*
 * Created on 10.04.2005
 */
import lisp.geometry.Picture;
import lisp.Constant;
import lisp.List;
import lisp.Rational;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.util.LinkedList;
/**
 * @author Andreasm
 */
final class PolylinesPicture extends Constant implements Picture
{
    private static final BasicStroke STROKE_1 = new BasicStroke(1f);
    private static final double STROKE_LIMIT = 0.9d;
    //
    private final double width;
    private final double height;
    private final LinkedList<Shape> shapes;
    //
    PolylinesPicture(double width, double height, List listlist)
    {
        super();
        //
        this.width = width;
        this.height = height;
        //
        shapes = new LinkedList<>();
        //
        while (listlist != null)
        {
            List list = (List) listlist.first();
            shapes.addLast(toShape(list, width, height));
            listlist = listlist.rest();
        }
    }
    //
    public String getType()
    {
        return "picture";
    }
    //
    public double getWidth()
    {
        return width;
    }
    //
    public double getHeight()
    {
        return height;
    }
    //
    public void paint(Graphics2D graphics, AffineTransform transform)
    {
        Color lastcolor = graphics.getColor();
        Stroke laststroke = graphics.getStroke();
        //
        try
        {
            graphics.setColor(Color.BLACK);
            graphics.setStroke(getStroke(transform));
            //
            for (Shape shape : shapes)
            {
                graphics.draw(transform.createTransformedShape(shape));
            }
        }
        finally
        {
            graphics.setStroke(laststroke);
            graphics.setColor(lastcolor);
        }
    }
    //
    private static BasicStroke getStroke(AffineTransform transform)
    {
        double determinant = Math.abs(transform.getDeterminant());
        double scale = Math.sqrt(determinant);
        //
        if (scale >= STROKE_LIMIT) return STROKE_1;
        //
        return new BasicStroke((float) (scale / STROKE_LIMIT));
    }
    //
    public Picture simplify()
    {
        return this;
    }
    //
    private static Shape toShape(List pairlist, double width, double height)
    {
        Path2D path = new Path2D.Double();
        double[] lastpoint = null;
        //
        if (pairlist != null)
        {
            List pair = (List) pairlist.first();
            lastpoint = toPoint(pair, width, height);
            path.moveTo(lastpoint[0], lastpoint[1]);
            pairlist = pairlist.rest();
        }
        //
        while (pairlist != null)
        {
            List pair = (List) pairlist.first();
            double[] point = toPoint(pair, width, height);
            //
            if (unequal(lastpoint, point)) path.lineTo(point[0], point[1]);
            //
            lastpoint = point;
            pairlist = pairlist.rest();
        }
        //
        return path;
    }
    //
    private static double[] toPoint(List pair, double width, double height)
    {
        Rational first = (Rational) pair.first();
        pair = pair.rest();
        Rational second = (Rational) pair.first();
        pair = pair.rest();
        //
        if (pair != null) throw new IllegalArgumentException("point list contains a list with more than two elements");
        //
        double x = first.doubleValue();
        double y = second.doubleValue();
        //
        if (x < 0d || y < 0d) throw new IllegalArgumentException("points must not have negative coordinates");
        //
        if (x > width || y > height) throw new IllegalArgumentException("points must be inside of bounds");
        //
        return new double[] { x, y };
    }
    //
    private static boolean unequal(double[] point1, double[] point2)
    {
        return point1[0] != point2[0] || point1[1] != point2[1];
    }
}