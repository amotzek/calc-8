package lisp.geometry.combinator;
/*
 * Copyright (C) 2005, 2014, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
import lisp.geometry.Picture;
import lisp.Constant;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
/*
 * @author Andreasm
 */
final class TransformedPicture extends Constant implements Picture
{
    private final AffineTransform transform;
    private final Picture picture;
    private final double width;
    private final double height;
    //
    TransformedPicture(AffineTransform transform, Picture picture, double width, double height)
    {
        super();
        //
        this.width = width;
        this.height = height;
        this.transform = transform;
        this.picture = picture;
    }
    //
    public String getType()
    {
        return "picture";
    }
    //
    public double getWidth()
    {
        return width;
    }
    //
    public double getHeight()
    {
        return height;
    }
    //
    public void paint(Graphics2D graphics, AffineTransform transform)
    {
        AffineTransform composedtransform = new AffineTransform(transform);
        composedtransform.concatenate(this.transform);
        picture.paint(graphics, composedtransform);
    }
    //
    public Picture simplify()
    {
        if (picture instanceof TransformedPicture)
        {
            TransformedPicture transformedpicture = (TransformedPicture) picture;
            AffineTransform composedtransform = new AffineTransform(transform);
            composedtransform.concatenate(transformedpicture.transform);
            //
            if (composedtransform.isIdentity()) return transformedpicture.picture;
            //
            return new TransformedPicture(composedtransform, transformedpicture.picture, width, height);
        }
        //
        if (picture instanceof OverlayPicture)
        {
            OverlayPicture overlaypicture = (OverlayPicture) picture;
            Picture transformedpicture = overlaypicture.applyTransform(transform, width, height);
            //
            if (transformedpicture != null) return transformedpicture;
        }
        //
        return this;
    }
}