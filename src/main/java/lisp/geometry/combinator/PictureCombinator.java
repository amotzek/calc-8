package lisp.geometry.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.geometry.Picture;
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.combinator.TypeCheckCombinator;
/*
 * Created by  andreasm 31.05.12 06:16
 */
abstract class PictureCombinator extends TypeCheckCombinator
{
    PictureCombinator(int quotemask, int parameters)
    {
        super(quotemask, parameters);
    }
    //
    Picture getPicture(Sexpression[] arguments, int index) throws CannotEvalException
    {
        Sexpression sexpr = arguments[index];
        //
        try
        {
            if (sexpr != null) return (Picture) sexpr;
        }
        catch (ClassCastException ignore)
        {
        }
        //
        throw new CannotEvalException(createErrorMessage("picture", sexpr));
    }
}