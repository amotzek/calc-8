package lisp.geometry.combinator;
/*
 * Copyright (C) 2005, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
import lisp.geometry.Picture;
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.awt.geom.AffineTransform;
/*
 * @author Andreasm
 */
final class RotateLeft extends PictureCombinator
{
    RotateLeft()
    {
        super(0, 1);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Picture picture = getPicture(arguments, 0);
        double width = picture.getWidth();
        double height = picture.getHeight();
        AffineTransform transform = new AffineTransform(0d, 1d, -1d, 0d, height, 0d);
        Picture rotatedpicture = new TransformedPicture(transform, picture, height, width);
        //
        return rotatedpicture.simplify();
    }
    //
    public String toString()
    {
        return "rotate-left";
    }
}