package lisp.geometry.combinator;
/*
 * Copyright (C) 2012, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 15.10.2012
 */
import lisp.geometry.Picture;
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.awt.geom.AffineTransform;
/*
 * @author Andreasm
 */
final class Scale extends PictureCombinator
{
    Scale()
    {
        super(0, 2);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        double scale = getRational(arguments, 0).doubleValue();
        Picture picture = getPicture(arguments, 1);
        //
        if (scale <= 0d) throw new CannotEvalException("scale must be positive");
        //
        double width = scale * picture.getWidth();
        double height = scale * picture.getHeight();
        AffineTransform transform = AffineTransform.getScaleInstance(scale, scale);
        Picture scaledpicture = new TransformedPicture(transform, picture, width, height);
        //
        return scaledpicture.simplify();
    }
    //
    public String toString()
    {
        return "scale";
    }
}