/*
 * Copyright (C) 2005, 2014 Andreas Motzek andreas-motzek@t-online.de
 * 
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
package lisp.geometry.combinator;
//
import lisp.geometry.Picture;
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.awt.geom.AffineTransform;
/*
 * @author Andreasm
 */
final class Beside extends PictureCombinator
{
    Beside()
    {
        super(0, 4);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        double ratioleft = getRational(arguments, 0).doubleValue();
        double ratioright = getRational(arguments, 1).doubleValue();
        Picture pictureleft = getPicture(arguments, 2);
        Picture pictureright = getPicture(arguments, 3);
        //
        if (ratioleft <= 0d || ratioright <= 0d) throw new CannotEvalException("ratios must be positive in beside");
        //
        double sourcewidthleft = pictureleft.getWidth();
        double sourceheightleft = pictureleft.getHeight();
        double sourcewidthright = pictureright.getWidth();
        double sourceheightright = pictureright.getHeight();
        //
        double width = sourcewidthleft + sourcewidthright;
        double height = Math.max(sourceheightleft, sourceheightright);
        //
        double ratio = ratioleft + ratioright;
        double inverseratio = 1d / ratio;
        //
        double destinationwidthleft = width * ratioleft * inverseratio;
        double destinationwidthright = width * ratioright * inverseratio;
        //
        double scaleleft = destinationwidthleft / sourcewidthleft;
        double scaleright = destinationwidthright / sourcewidthright;
        double scaletop = height / sourceheightleft;
        double scalebottom = height / sourceheightright;
        //
        AffineTransform transformleft = AffineTransform.getScaleInstance(scaleleft, scaletop);
        AffineTransform transformright = new AffineTransform(scaleright, 0d, 0d, scalebottom, destinationwidthleft, 0d);
        TransformedPicture transformedleft = new TransformedPicture(transformleft, pictureleft, destinationwidthleft, height);
        TransformedPicture transformedright = new TransformedPicture(transformright, pictureright, destinationwidthright, height);
        OverlayPicture overlaypicture = new OverlayPicture(transformedleft.simplify(), transformedright.simplify(), width, height);
        //
        return overlaypicture.simplify();
    }
    //
    public String toString()
    {
        return "beside";
    }
}