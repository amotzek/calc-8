package lisp.geometry.combinator;
/*
 * Copyright (C) 2005, 2014, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
final class Grid extends TypeCheckCombinator
{
    Grid()
    {
        super(0, 3);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        double width = getRational(arguments, 0).doubleValue();
        double height = getRational(arguments, 1).doubleValue();
        List list = getList(arguments, 2);
        //
        try
        {
            return new PolylinesPicture(width, height, list);
        }
        catch (NullPointerException e)
        {
            throw new CannotEvalException("not a list of lists of pairs");
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException("not a list of lists of number pairs");
        }
        catch (IllegalArgumentException e)
        {
            throw new CannotEvalException(e.getMessage());
        }
    }
    //
    public String toString()
    {
        return "grid";
    }
}