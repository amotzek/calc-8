package lisp.geometry.combinator;
/*
 * Copyright (C) 2005, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 10.04.2005
 */
import lisp.geometry.Picture;
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
final class Overlay extends PictureCombinator
{
    Overlay()
    {
        super(0, 2);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Picture picture1 = getPicture(arguments, 0);
        Picture picture2 = getPicture(arguments, 1);
        double height = Math.max(picture1.getHeight(), picture2.getHeight());
        double width = Math.max(picture1.getWidth(), picture2.getWidth());
        OverlayPicture overlaypicture = new OverlayPicture(picture1, picture2, width, height);
        //
        return overlaypicture.simplify();
    }
    //
    public String toString()
    {
        return "overlay";
    }
}