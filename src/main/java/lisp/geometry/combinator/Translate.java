package lisp.geometry.combinator;
/*
 * Copyright (C) 2012, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Functional Geometry package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 15.10.2012
 */
import lisp.geometry.Picture;
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.awt.geom.AffineTransform;
/*
 * @author Andreasm
 */
final class Translate extends PictureCombinator
{
    Translate()
    {
        super(0, 3);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        double right = getRational(arguments, 0).doubleValue();
        double bottom = getRational(arguments, 1).doubleValue();
        Picture picture = getPicture(arguments, 2);
        //
        if (right < 0d || bottom < 0d) throw new CannotEvalException("translation must not have a negative component");
        //
        double width = picture.getWidth();
        double height = picture.getHeight();
        AffineTransform transform = AffineTransform.getTranslateInstance(right, bottom);
        Picture translatedpicture = new TransformedPicture(transform, picture, width + right, height + bottom);
        //
        return translatedpicture.simplify();
    }
    //
    public String toString()
    {
        return "translate";
    }
}