package calc.io;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.module.Module;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.StringTokenizer;
/*
 * Created by Andreas on 25.09.2018.
 */
public final class Session
{
    private static final String FORM_DATA = "application/x-www-form-urlencoded";
    private static final String JSON = "text/json; charset=UTF-8";
    //
    private final String server;
    private String cookie;
    //
    public Session(String server)
    {
        super();
        //
        this.server = server;
    }
    //
    public void login(String emailaddress, char[] password) throws IOException
    {
        String content = String.format("e-mail-address=%s&password=%s", encode(emailaddress), encode(new String(password)));
        JSONObject result = (JSONObject) post("login.object", FORM_DATA, content);
        //
        try
        {
            String status = result.getString("status");
            //
            if ("nil".equals(status)) throw new IllegalArgumentException("Credentials not correct");
            //
            if (!"active".equals(status)) throw new IllegalStateException(String.format("Cannot log in (%s)", status));
        }
        catch (JSONException e)
        {
            throw new IOException(e);
        }
    }
    //
    public void pushModule(String name, String commentde, Collection<String> exports, String body, Collection<Module> dependencies) throws IOException
    {
        try
        {
            JSONObject moduleobject = new JSONObject();
            moduleobject.put("name", name);
            moduleobject.put("comment-de", commentde);
            moduleobject.put("exports", exports);
            moduleobject.put("body", body);
            //
            for (Module dependency : dependencies)
            {
                JSONObject dependencyobject = new JSONObject();
                dependencyobject.put("name", dependency.getName());
                dependencyobject.put("version", dependency.getVersion());
                moduleobject.append("dependencies", dependencyobject);
            }
            //
            post("module/push.object", JSON, moduleobject.toString());
        }
        catch (JSONException e)
        {
            throw new IllegalArgumentException(e);
        }
    }
    //
    public void logout() throws IOException
    {
        post("logout.object", JSON, "{}");
    }
    /*
    private Object get(String resource) throws IOException
    {
        URL url = new URL(String.format("%s/%s", server, resource));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        connection.setDoOutput(false);
        //
        if (cookie != null) connection.setRequestProperty("Cookie", cookie);
        //
        String content = new String(read(connection), "UTF-8");
        JSONTokener tokener = new JSONTokener(content);
        //
        try
        {
            return tokener.nextValue();
        }
        catch (JSONException e)
        {
            throw new IOException(e);
        }
    }
    */
    private Object post(String resource, String contenttype, String content) throws IOException
    {
        URL url = new URL(String.format("%s/%s", server, resource));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        //
        if (cookie != null) connection.setRequestProperty("Cookie", cookie);
        //
        if (content != null)
        {
            byte[] bytes = content.getBytes("UTF-8");
            connection.setRequestProperty("Content-Length", Integer.toString(bytes.length));
            connection.setRequestProperty("Content-Type", contenttype);
            connection.setDoOutput(true);
            //
            try (OutputStream stream = connection.getOutputStream())
            {
                stream.write(bytes);
            }
        }
        //
        content = new String(read(connection), "UTF-8");
        JSONTokener tokener = new JSONTokener(content);
        //
        try
        {
            return tokener.nextValue();
        }
        catch (JSONException e)
        {
            throw new IOException(e);
        }
    }
    //
    private byte[] read(HttpURLConnection connection) throws IOException
    {
        int code = connection.getResponseCode();
        //
        if (code > 299)
        {
            try (InputStream stream = connection.getErrorStream(); InputStreamReader reader = new InputStreamReader(stream))
            {
                StringBuilder message = new StringBuilder();
                //
                for (int r = reader.read(); r >= 0; r = reader.read())
                {
                    message.append((char) r);
                }
                //
                throw new IOException(toPlainText(message.toString()));
            }
        }
        //
        String nextcookie = connection.getHeaderField("Set-Cookie");
        //
        if (nextcookie != null) cookie = nextcookie;
        //
        int length = connection.getHeaderFieldInt("Content-Length", 0);
        //
        if (length == 0) return new byte[0];
        //
        byte[] bytes = new byte[length];
        int offset = 0;
        //
        try (InputStream stream = connection.getInputStream())
        {
            do
            {
                int count = stream.read(bytes, offset, length);
                //
                if (count <= 0) throw new IOException("cannot read");
                //
                offset += count;
                length -= count;
            }
            while (length > 0);
        }
        //
        return bytes;
    }
    //
    private static String encode(String value) throws IOException
    {
        return URLEncoder.encode(value, "UTF-8");
    }
    //
    private static String toPlainText(String html)
    {
        StringTokenizer tokenizer = new StringTokenizer(html, "<>&; ", true);
        StringBuilder text = new StringBuilder();
        boolean intag = false;
        //
        while (tokenizer.hasMoreTokens())
        {
            String token = tokenizer.nextToken();
            //
            if (intag)
            {
                if (">".equals(token))
                {
                    if (hasNotBlankEnd(text)) text.append(' ');
                    //
                    intag = false;
                }
            }
            else if ("<".equals(token))
            {
                intag = true;
            }
            else if ("&".equals(token))
            {
                String entity = tokenizer.nextToken();
                //
                if (!";".equals(tokenizer.nextToken())) break;
                //
                switch (entity)
                {
                    case "gt": text.append(">"); break;
                    case "lt": text.append("<"); break;
                    default:
                    {
                        if (entity.startsWith("#"))
                        {
                            try
                            {
                                text.append((char) Integer.parseInt(entity.substring(1)));
                            }
                            catch (NumberFormatException ignore)
                            {
                            }
                        }
                    }
                }
            }
            else if (" ".equals(token))
            {
                if (hasNotBlankEnd(text)) text.append(' ');
            }
            else
            {
                text.append(token);
            }
        }
        //
        return text.toString();
    }
    //
    private static boolean hasNotBlankEnd(StringBuilder builder)
    {
        int length = builder.length();
        //
        if (length == 0) return false;
        //
        char last = builder.charAt(length - 1);
        //
        return last != ' ' && last != '\n';
    }
}
