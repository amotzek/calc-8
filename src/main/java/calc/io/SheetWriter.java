package calc.io;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collection;
import calc.model.Cell;
import calc.model.Column;
import calc.model.Row;
import calc.model.Sheet;
import lisp.module.Module;
import propertyset.PropertySet;
import xml.propertyset.PropertySetFormatter;
/*
 * Created by  andreasm 22.05.12 19:37
 */
public final class SheetWriter extends SheetOrModuleWriter
{
    private String xml;
    //
    public SheetWriter(Sheet sheet)
    {
        super(sheet);
    }
    //
    public void run()
    {
        PropertySet propertyset = toPropertySet(sheet);
        PropertySetFormatter formatter = new PropertySetFormatter(propertyset);
        xml = formatter.getXML();
    }
    //
    @SuppressWarnings("unused")
    public String getXML()
    {
        return xml;
    }
    //
    public void writeTo(File file) throws IOException
    {
        boolean wasdefault = false;
        //
        if (file == null)
        {
            file = getDefaultFile();
            wasdefault = true;
        }
        //
        FileOutputStream stream = new FileOutputStream(file);
        OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8");
        writer.write(xml);
        writer.flush();
        writer.close();
        //
        if (wasdefault) return;
        //
        sheet.setUnchanged();
    }
    //
    private static PropertySet toPropertySet(Sheet sheet)
    {
        PropertySet propertyset = new PropertySet();
        propertyset.setName("sheet");
        Collection<Cell> cells = sheet.getCells();
        //
        for (Cell cell : cells)
        {
            if (cell.hasFormula()) propertyset.addChild(toPropertySet(cell));
        }
        //
        String program = sheet.getProgram();
        //
        if (program != null) propertyset.addChild("program", program);
        //
        Collection<Module> modules = sheet.getModules();
        String modulename = sheet.getModuleName();
        String modulecommentde = sheet.getModuleCommentDe();
        Collection<String> moduleexports = sheet.getModuleExports();
        //
        for (Module module : modules)
        {
            propertyset.addChild(toPropertySet(module));
        }
        //
        if (modulename != null) propertyset.addChild("module-name", modulename);
        //
        if (modulecommentde != null) propertyset.addChild("module-description", modulecommentde); // TODO umbenennen
        //
        if (moduleexports != null) propertyset.addChild("module-exports", toString(moduleexports)); // TODO einzelne Elemente erzeugen
        //
        return propertyset;
    }
    //
    private static PropertySet toPropertySet(Cell cell)
    {
        Column column = cell.getColumn();
        Row row = cell.getRow();
        String formula = cell.getFormula();
        boolean locked = cell.isLocked();
        PropertySet propertyset = new PropertySet();
        propertyset.setName("cell");
        propertyset.addChild("column", column.getLabel());
        propertyset.addChild("row", row.getLabel());
        propertyset.addChild("formula", formula);
        //
        if (locked) propertyset.addChild("locked", null);
        //
        return propertyset;
    }
    //
    private static String toString(Collection<String> exports)
    {
        StringBuilder builder = new StringBuilder();
        //
        for (String export : exports)
        {
            if (builder.length() > 0) builder.append(' ');
            //
            builder.append(export);
        }
        //
        return builder.toString();
    }
}