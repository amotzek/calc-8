package calc.io;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.module.Module;
import propertyset.ObjectStructure;
import propertyset.PropertySet;
import propertyset.VisitorException;
import xml.propertyset.PropertySetParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
/**
 * Created by andreasm on 07.06.15
 */
public final class ModuleReader
{
    private String cookie;
    //
    public ModuleReader()
    {
        super();
    }
    //
    public Module readFrom(URI uri) throws IOException, VisitorException
    {
        URL url = uri.toURL();
        URLConnection connection = url.openConnection();
        restoreCookie(connection);
        PropertySetParser parser = createParser(connection);
        saveCookie(connection);
        //
        return parseModule(parser);
    }
    //
    private void restoreCookie(URLConnection connection)
    {
        if (cookie == null) return;
        //
        connection.setRequestProperty("Cookie", cookie);
    }
    //
    private PropertySetParser createParser(URLConnection connection) throws IOException
    {
        PropertySetParser parser;
        //
        try (InputStream stream = connection.getInputStream())
        {
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            parser = new PropertySetParser(reader);
        }
        //
        return parser;
    }
    //
    private void saveCookie(URLConnection connection)
    {
        String cookie = connection.getHeaderField("Set-Cookie");
        //
        if (cookie != null) this.cookie = cookie;
    }
    //
    private Module parseModule(PropertySetParser parser) throws VisitorException
    {
        PropertySet propertyset = parser.getPropertySet();
        ModuleVisitor visitor = new ModuleVisitor();
        ObjectStructure structure = new ObjectStructure(propertyset);
        structure.traverseWith(visitor);
        //
        return visitor.getModule();
    }
}