package calc.io;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Cell;
import calc.model.Sheet;
import propertyset.PropertySet;
import propertyset.Visitor;
import propertyset.VisitorException;
/*
 * Created by  andreasm 22.05.12 20:52
 */
final class CellVisitor implements Visitor
{
    private final Sheet sheet;
    private final long lastupdate;
    //
    public CellVisitor(Sheet sheet, long lastupdate)
    {
        super();
        //
        this.sheet = sheet;
        this.lastupdate = lastupdate;
    }
    //
    public int enter(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName("sheet")) return DEPTH_FIRST;
        //
        if (propertyset.hasName("cell"))
        {
            PropertySet childcolumn = propertyset.getChild("column");
            PropertySet childrow = propertyset.getChild("row");
            PropertySet childformula = propertyset.getChild("formula");
            PropertySet childlocked = propertyset.getChild("locked");
            //
            if (childcolumn == null) throw new VisitorException("cell without column");
            //
            if (childrow == null) throw new VisitorException("cell without row");
            //
            if (childformula == null) throw new VisitorException("cell without formula");
            //
            try
            {
                String columnlabel = childcolumn.getValue();
                int rownumber = Integer.parseInt(childrow.getValue());
                String formula = childformula.getValue();
                Cell cell = sheet.findCell(columnlabel, rownumber);
                cell.setFormula(formula, lastupdate);
                cell.setLocked(childlocked != null);
            }
            catch (NumberFormatException e)
            {
                throw new VisitorException(e.getMessage());
            }
        }
        //
        return LEAF;
    }
    //
    public void leave(PropertySet propertyset) throws VisitorException
    {
    }
}