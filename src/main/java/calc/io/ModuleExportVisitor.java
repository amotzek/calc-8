package calc.io;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Sheet;
import propertyset.PropertySet;
import propertyset.Visitor;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.StringTokenizer;
/**
 * Created by andreasm on 10.06.15
 */
final class ModuleExportVisitor implements Visitor
{
    private final Sheet sheet;
    //
    ModuleExportVisitor(Sheet sheet)
    {
        super();
        //
        this.sheet = sheet;
    }
    //
    public int enter(PropertySet propertyset)
    {
        if (propertyset.hasName("sheet")) return DEPTH_FIRST;
        //
        if (propertyset.hasName("module-name")) sheet.setModuleName(propertyset.getValue());
        //
        if (propertyset.hasName("module-description")) sheet.setModuleCommentDe(propertyset.getValue());
        //
        if (propertyset.hasName("module-exports")) sheet.setModuleExports(toCollection(propertyset.getValue()));
        //
        return LEAF;
    }
    //
    public void leave(PropertySet propertyset)
    {
    }
    //
    private static Collection<String> toCollection(String exports)
    {
        if (exports == null) return Collections.emptyList();
        //
        StringTokenizer tokenizer = new StringTokenizer(exports, " ", false);
        LinkedList<String> collection = new LinkedList<>();
        //
        while (tokenizer.hasMoreTokens())
        {
            collection.addLast(tokenizer.nextToken());
        }
        //
        return collection;
    }
}