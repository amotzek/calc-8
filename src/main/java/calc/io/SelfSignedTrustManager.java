package calc.io;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Circuit Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.security.auth.x500.X500Principal;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
/*
 * Created by Andreas on 23.11.2017.
 */
public final class SelfSignedTrustManager extends X509ExtendedTrustManager
{
    private X509ExtendedTrustManager delegate;
    //
    public SelfSignedTrustManager() throws GeneralSecurityException
    {
        TrustManagerFactory factory = TrustManagerFactory.getInstance("PKIX");
        factory.init((KeyStore) null); // default KeyStore https://stackoverflow.com/questions/23144353
        TrustManager[] trustmanagers = factory.getTrustManagers();
        //
        for (TrustManager trustmanager : trustmanagers)
        {
            if (trustmanager instanceof X509ExtendedTrustManager)
            {
                delegate = (X509ExtendedTrustManager) trustmanager;
                //
                break;
            }
        }
    }
    //
    @Override
    public void checkServerTrusted(X509Certificate[] certificates, String authtype, Socket socket) throws CertificateException
    {
        if (certificates.length == 1)
        {
            X509Certificate certificate = certificates[0];
            X500Principal issuer = certificate.getIssuerX500Principal();
            X500Principal subject = certificate.getSubjectX500Principal();
            //
            if (issuer.equals(subject)) return;
        }
        //
        delegate.checkServerTrusted(certificates, authtype, socket);
    }
    //
    @Override
    public void checkServerTrusted(X509Certificate[] certificates, String authtype) throws CertificateException
    {
        delegate.checkServerTrusted(certificates, authtype);
    }
    //
    @Override
    public void checkServerTrusted(X509Certificate[] certificates, String authtype, SSLEngine engine) throws CertificateException
    {
        delegate.checkServerTrusted(certificates, authtype, engine);
    }
    //
    @Override
    public X509Certificate[] getAcceptedIssuers()
    {
        return delegate.getAcceptedIssuers();
    }
    //
    @Override
    public void checkClientTrusted(X509Certificate[] certificates, String authtype, Socket socket)
    {
        throw new UnsupportedOperationException();
    }
    //
    @Override
    public void checkClientTrusted(X509Certificate[] certificates, String authtype, SSLEngine engine)
    {
        throw new UnsupportedOperationException();
    }
    //
    @Override
    public void checkClientTrusted(X509Certificate[] certificates, String authtype)
    {
        throw new UnsupportedOperationException();
    }
}
