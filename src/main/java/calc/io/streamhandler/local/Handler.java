package calc.io.streamhandler.local;
/*
 * Copyright (C) 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
/*
 * Created by andreasm on 30.11.2016.
 */
public final class Handler extends URLStreamHandler
{
    public Handler()
    {
        super();
    }
    //
    @Override
    protected URLConnection openConnection(URL url) throws IOException
    {
        return new FileURLConnection(url);
    }
}
