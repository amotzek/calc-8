package calc.io.streamhandler.local;
/*
 * Copyright (C) 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
/*
 * Created by andreasm on 30.11.2016.
 */
final class FileURLConnection extends URLConnection
{
    private File file;
    //
    public FileURLConnection(URL url)
    {
        super(url);
    }
    //
    @Override
    public void connect() throws IOException
    {
        if (connected) return;
        //
        String name = url.getFile();
        String separator = System.getProperty("file.separator");
        String home = System.getProperty("user.home");
        //
        if (name.contains(".") || name.contains("/") || name.contains(separator)) throw new IOException("illegal module name: " + name);
        //
        file = new File(String.format("%s/.calc-modules/%s.module.xml", home, name));
        connected = true;
    }
    //
    @Override
    public long getContentLengthLong()
    {
        if (!connected) throw new IllegalStateException("not connected");
        //
        return file.length();
    }
    //
    @Override
    public long getLastModified()
    {
        if (!connected) throw new IllegalStateException("not connected");
        //
        return file.lastModified();
    }
    //
    @Override
    public InputStream getInputStream() throws IOException
    {
        connect();
        //
        return new FileInputStream(file);
    }
    //
    @Override
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public OutputStream getOutputStream() throws IOException
    {
        String home = System.getProperty("user.home");
        File modulesdirectory = new File(home, "/.calc-modules");
        modulesdirectory.mkdir();
        connect();
        //
        return new FileOutputStream(file);
    }
}
