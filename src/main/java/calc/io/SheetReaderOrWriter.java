package calc.io;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import calc.model.Sheet;
/*
 * Created by  andreasm 03.06.12 16:01
 */
class SheetReaderOrWriter
{
    protected final Sheet sheet;
    //
    public SheetReaderOrWriter(Sheet sheet)
    {
        super();
        //
        this.sheet = sheet;
    }
    //
    protected static File getDefaultFile()
    {
        String home = System.getProperty("user.home");
        //
        return new File(home, ".sheet.xml");
    }
}