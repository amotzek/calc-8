package calc.io;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.module.Module;
import lisp.module.ModuleDependency;
import propertyset.PropertySet;
import propertyset.Visitor;
import propertyset.VisitorException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.StringTokenizer;
/**
 * Created by andreasm on 07.06.15
 */
final class ModuleVisitor implements Visitor
{
    private final LinkedList<Module> modules;
    private final LinkedList<ModuleDependency> dependencies;
    private final LinkedList<String> moduleexports;
    private boolean inmodule;
    private boolean independency;
    private String modulename;
    private Integer moduleversion;
    private URI moduleuri;
    private String modulecommentde;
    private String modulecommenten;
    private String modulebody;
    private String dependencyname;
    private Integer depedencyversion;
    private URI depedencyuri;
    //
    ModuleVisitor()
    {
        super();
        //
        modules = new LinkedList<>();
        dependencies = new LinkedList<>();
        moduleexports = new LinkedList<>();
    }
    //
    public int enter(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName("sheet")) return DEPTH_FIRST;
        //
        if (propertyset.hasName("module"))
        {
            inmodule = true;
            //
            return DEPTH_FIRST;
        }
        //
        if (inmodule)
        {
            if (propertyset.hasName("name"))
            {
                if (independency)
                {
                    dependencyname = propertyset.getValue();
                }
                else
                {
                    modulename = propertyset.getValue();
                }
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("version"))
            {
                moduleversion = getIntegerValue(propertyset);
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("uri") || propertyset.hasName("url"))
            {
                if (independency)
                {
                    depedencyuri = getURIValue(propertyset);
                }
                else
                {
                    moduleuri = getURIValue(propertyset);
                }
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("comment-de") || propertyset.hasName("description"))
            {
                modulecommentde = propertyset.getValue();
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("comment-en"))
            {
                modulecommenten = propertyset.getValue();
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("export"))
            {
                moduleexports.addLast(propertyset.getValue());
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("exports"))
            {
                StringTokenizer tokenizer = new StringTokenizer(propertyset.getValue(), " ", false);
                //
                while (tokenizer.hasMoreTokens())
                {
                    moduleexports.addLast(tokenizer.nextToken());
                }
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("body"))
            {
                modulebody = propertyset.getValue();
                //
                return LEAF;
            }
            //
            if (propertyset.hasName("dependency"))
            {
                independency = true;
                //
                return DEPTH_FIRST;
            }
            //
            if (propertyset.hasName("minimum-required-version"))
            {
                depedencyversion = getIntegerValue(propertyset);
                //
                return LEAF;
            }
        }
        //
        return LEAF;
    }
    //
    public void leave(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName("module"))
        {
            Module module = new Module(modulename, moduleversion, moduleuri, modulecommentde, modulecommenten, new ArrayList<>(moduleexports), modulebody, new ArrayList<>(dependencies));
            modules.addLast(module);
            modulename = null;
            moduleversion = null;
            moduleuri = null;
            modulecommentde = null;
            modulecommenten = null;
            moduleexports.clear();
            modulebody = null;
            dependencies.clear();
            inmodule = false;
        }
        //
        if (propertyset.hasName("dependency"))
        {
            ModuleDependency dependency = new ModuleDependency(dependencyname, depedencyversion, depedencyuri, null);
            dependencies.addLast(dependency);
            dependencyname = null;
            depedencyversion = null;
            depedencyuri = null;
            independency = false;
        }
    }
    //
    public Module getModule() throws VisitorException
    {
        if (modules.isEmpty()) throw new VisitorException("no module");
        //
        if (modules.size() > 1) throw new VisitorException("more than one module");
        //
        return modules.getFirst();
    }
    //
    public Collection<Module> getModules()
    {
        return new ArrayList<>(modules);
    }
    //
    private static Integer getIntegerValue(PropertySet propertyset) throws VisitorException
    {
        try
        {
            return new Integer(propertyset.getValue());
        }
        catch (NumberFormatException e)
        {
            throw new VisitorException("version is not a valid integer");
        }
    }
    //
    private static URI getURIValue(PropertySet propertyset) throws VisitorException
    {
        String value = propertyset.getValue();
        //
        try
        {
            return new URI(value);
        }
        catch (URISyntaxException e)
        {
            throw new VisitorException(String.format("uri or url %s is not valid", value));
        }
    }
}