package calc.io;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import calc.model.Log;
import calc.model.Sheet;
import propertyset.ObjectStructure;
import propertyset.PropertySet;
import propertyset.VisitorException;
import xml.propertyset.PropertySetParser;
/*
 * Created by  andreasm 22.05.12 20:41
 */
public final class SheetReader extends SheetReaderOrWriter
{
    private final Log log;
    private PropertySetParser parser;
    private long lastupdate;
    //
    public SheetReader(Sheet sheet, Log log)
    {
        super(sheet);
        //
        this.log = log;
    }
    //
    public void readFrom(File file) throws IOException
    {
        if (file == null) file = getDefaultFile();
        //
        lastupdate = file.lastModified();
        FileInputStream stream = new FileInputStream(file);
        //
        try (InputStreamReader reader = new InputStreamReader(stream, "UTF-8"))
        {
            parser = new PropertySetParser(reader);
        }
    }
    //
    public void run() throws VisitorException, IOException
    {
        PropertySet propertyset = parser.getPropertySet();
        CellVisitor cellvisitor = new CellVisitor(sheet, lastupdate);
        ProgramVisitor programvisitor = new ProgramVisitor(sheet);
        ModuleVisitor modulevisitor = new ModuleVisitor();
        ModuleExportVisitor moduleexportvisitor = new ModuleExportVisitor(sheet);
        ObjectStructure structure = new ObjectStructure(propertyset);
        sheet.clear();
        log.clear();
        structure.traverseWith(moduleexportvisitor);
        structure.traverseWith(modulevisitor);
        sheet.setModules(modulevisitor.getModules());
        structure.traverseWith(programvisitor);
        structure.traverseWith(cellvisitor);
        sheet.setUnchanged();
    }
}