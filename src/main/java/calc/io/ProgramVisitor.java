package calc.io;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Sheet;
import propertyset.PropertySet;
import propertyset.Visitor;
import propertyset.VisitorException;
/*
 * Created by andreasm on 29.10.12 at 07:51
 */
final class ProgramVisitor implements Visitor
{
    private final Sheet sheet;
    //
    public ProgramVisitor(Sheet sheet)
    {
        super();
        //
        this.sheet = sheet;
    }
    //
    public int enter(PropertySet propertyset) throws VisitorException
    {
        if (propertyset.hasName("sheet")) return DEPTH_FIRST;
        //
        if (propertyset.hasName("library") || propertyset.hasName("program"))
        {
            String program = propertyset.getValue();
            sheet.setProgram(program);
        }
        //
        return LEAF;
    }
    //
    public void leave(PropertySet propertyset) throws VisitorException
    {
    }
}