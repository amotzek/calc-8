package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.ViewMode;
import calc.view.ProgramPanel;
/*
 * Created by andreasm 13.02.13 22:51
 */
public final class ShowProgram extends AbstractMediatorAction
{
    public ShowProgram(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("SHOW_PROGRAM") + "...   ");
    }
    //
    public void actionPerformed(ActionEvent e)
    {
        ViewMode viewmode = mediator.getSingleton(ViewMode.class);
        ProgramPanel programpanel = mediator.getSingleton(ProgramPanel.class);
        programpanel.copyFromSheet();
        viewmode.setProgramMode();
    }
}