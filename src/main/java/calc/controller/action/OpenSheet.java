package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.controller.command.ReadSheet;
import calc.model.Timeline;
import static calc.view.Style.*;
/*
 * Created by andreasm 13.02.13 22:11
 */
public final class OpenSheet extends FileAction
{
    public OpenSheet(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("OPEN") + "...   ");
        putValue(ACCELERATOR_KEY, createAccelerator(KeyEvent.VK_F));
    }
    //
    @Override
    protected Runnable getRunnable()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        //
        if (keepUnsavedChanges(bundle.getString("WANT_OPEN_SHEET"))) return null;
        //
        File file = chooseFile(false, bundle.getString("OPEN"));
        //
        if (file == null) return null;
        //
        Timeline timeline = mediator.getSingleton(Timeline.class);
        timeline.clear();
        //
        return mediator.createAndPostConstructObject(ReadSheet.class, file);
    }
    //
    public void run()
    {
        assert false;
    }
}