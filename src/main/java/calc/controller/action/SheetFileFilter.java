package calc.controller.action;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.util.ResourceBundle;
import javax.swing.filechooser.FileFilter;
import calc.Mediator;
/*
 * Created by  andreasm 02.06.12 20:34
 */
public final class SheetFileFilter extends FileFilter
{
    private static final String EXTENSION = ".sheet.xml";
    //
    private final Mediator mediator;
    //
    public SheetFileFilter(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @Override
    public boolean accept(File pathname)
    {
        if (pathname.isDirectory()) return true;
        //
        String name = pathname.getName();
        //
        return name.endsWith(EXTENSION);
    }
    //
    @Override
    public String getDescription()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        //
        return bundle.getString("FILE_DESCRIPTION");
    }
    //
    public File normalize(File file)
    {
        if (accept(file)) return file;
        //
        String path = file.getAbsolutePath();
        //
        return new File(path + EXTENSION);
    }
}