package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import calc.Mediator;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
/*
 * Created by andreasm 13.02.13 21:35
 */
abstract class ConcurrentAction extends AbstractMediatorAction implements Runnable
{
    protected ConcurrentAction(Mediator mediator)
    {
        super(mediator);
    }
    //
    protected Runnable getRunnable()
    {
        return this;
    }
    //
    public final void actionPerformed(ActionEvent event)
    {
        Runnable runnable = getRunnable();
        //
        if (runnable == null) return;
        //
        RunnableQueue runnablequeue = RunnableQueueFactory.getConcurrentRunnableQueue();
        runnablequeue.add(runnable);
    }
}