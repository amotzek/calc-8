package calc.controller.action;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import javax.swing.Action;
import calc.model.Selection;
import calc.model.Sheet;
/*
 * Created by andreasm on 28.05.12 at 13:24
 */
public abstract class ChainableCellAction extends CellAction
{
    private Action next;
    //
    public ChainableCellAction(Sheet sheet, Selection selection)
    {
        super(sheet, selection);
    }
    //
    protected final Action getNextAction()
    {
        return next;
    }
    //
    public final void setNextAction(Action next)
    {
        this.next = next;
    }
}