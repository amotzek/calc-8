package calc.controller.action;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import javax.swing.Action;
import calc.model.Cell;
import calc.model.Selection;
import calc.model.Sheet;
import calc.view.FormulaTextArea;
/*
 * Created by andreasm on 28.05.12 at 13:28
 */
public final class CellOrCaretUp extends ChainableCellAction
{
    public CellOrCaretUp(Sheet sheet, Selection selection)
    {
        super(sheet, selection);
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        try
        {
            FormulaTextArea textarea = (FormulaTextArea) event.getSource();
            int position = textarea.getCaretPosition();
            int linenumber = textarea.getLineOfOffset(position);
            //
            if (linenumber > 0)
            {
                Action next = getNextAction();
                next.actionPerformed(event);
                //
                return;
            }
            //
            Cell cell = textarea.getCell();
            moveSelectedCell(cell, 0, -1);
        }
        catch (Exception e)
        {
            // don't move
        }
    }
}