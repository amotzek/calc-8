package calc.controller.action;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.controller.ModuleSaveController;
import calc.controller.command.PushModule;
import calc.controller.command.WriteModule;
import calc.model.SaveDestination;
import calc.model.ServiceModel;
import calc.model.Sheet;
import calc.model.WizardModel;
import calc.view.ModuleSaveDialog;
import java.util.ResourceBundle;
/**
 * Created by andreasm on 10.06.15
 */
public final class SaveModule extends ConcurrentAction
{
    public SaveModule(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("SAVE_MODULE") + "...");
    }
    //
    @Override
    protected Runnable getRunnable()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        ServiceModel servicemodel = mediator.getSingleton(ServiceModel.class);
        WizardModel wizardmodel = new WizardModel();
        ModuleSaveDialog dialog = mediator.createAndPostConstructObject(ModuleSaveDialog.class, wizardmodel);
        ModuleSaveController controller = new ModuleSaveController(wizardmodel, dialog);
        controller.connect();
        dialog.setVisible(true);
        String step = wizardmodel.getStep();
        //
        if (ModuleSaveDialog.DONE_STEP.equals(step))
        {
            sheet.setModuleName(dialog.getName());
            sheet.setModuleCommentDe(dialog.getCommentDe());
            sheet.setModuleExports(dialog.getExports());
            SaveDestination destination = dialog.getSaveDestination();
            //
            if (destination == null) return null;
            //
            switch (destination)
            {
                case LOCAL:
                    return mediator.createAndPostConstructObject(WriteModule.class);
                    //
                case CLOUD:
                {
                    servicemodel.setServer(dialog.getServer());
                    servicemodel.setEmailAddress(dialog.getEmailAddress());
                    servicemodel.setPassword(dialog.getPassword());
                    //
                    return mediator.createAndPostConstructObject(PushModule.class);
                }
            }
        }
        //
        return null;
    }
    //
    public void run()
    {
        assert false;
    }
}