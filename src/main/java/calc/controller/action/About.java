package calc.controller.action;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import calc.Mediator;
import calc.view.AppFrame;
import static calc.view.Style.*;
/*
 * Created by andreasm 13.02.13 21:20
 */
public final class About extends AbstractMediatorAction
{
    public About(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("ABOUT_CALC") + "...   ");
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        AppFrame appframe = mediator.getSingleton(AppFrame.class);
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        ImageIcon icon = getImageIcon("lisplogo.png");
        StringBuilder message = new StringBuilder();
        message.append(bundle.getString("DESCRIBE_CALC"));
        message.append("\n\n");
        message.append("Revision ");
        message.append(bundle.getString("REVISION_NUMBER"));
        message.append("\n");
        message.append(bundle.getString("REVISION_DATE"));
        message.append("\n");
        message.append("andreas-motzek@t-online.de\n\n");
        message.append("Java ");
        message.append(System.getProperty("java.version"));
        message.append(" ");
        message.append(System.getProperty("os.name"));
        message.append(" ");
        message.append(System.getProperty("os.arch"));
        message.append("\n");
        message.append(bundle.getString("LICENSE"));
        JOptionPane.showMessageDialog(appframe, message.toString(), bundle.getString("ABOUT_CALC"), JOptionPane.INFORMATION_MESSAGE, icon);
    }
}