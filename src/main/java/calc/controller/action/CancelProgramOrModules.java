package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.ViewMode;
/*
 * Created by andreasm 25.02.13 19:23
 */
public final class CancelProgramOrModules extends AbstractMediatorAction
{
    public CancelProgramOrModules(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("CANCEL"));
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        ViewMode viewmode = mediator.getSingleton(ViewMode.class);
        viewmode.setSheetMode();
    }
}