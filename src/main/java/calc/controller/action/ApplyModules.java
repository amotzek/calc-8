package calc.controller.action;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.model.ViewMode;
import calc.view.ModulesPanel;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
/*
 * Created by andreasm 01.06.15 22:30
 */
public final class ApplyModules extends AbstractMediatorAction
{
    public ApplyModules(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("APPLY"));
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        ViewMode viewmode = mediator.getSingleton(ViewMode.class);
        ModulesPanel modulespanel = mediator.getSingleton(ModulesPanel.class);
        modulespanel.copyToSheet();
        viewmode.setSheetMode();
    }
}