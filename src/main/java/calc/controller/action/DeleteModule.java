package calc.controller.action;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.view.ModulePanel;
import javax.swing.AbstractAction;
import java.awt.Container;
import java.awt.event.ActionEvent;
import static calc.view.Style.getImageIcon;
/**
 * Created by andreasm on 09.06.15
 */
public final class DeleteModule extends AbstractAction
{
    private final ModulePanel modulepanel;
    //
    public DeleteModule(ModulePanel modulepanel)
    {
        super();
        //
        this.modulepanel = modulepanel;
        //
        putValue(SMALL_ICON, getImageIcon("delete.png"));
    }
    //
    public void actionPerformed(ActionEvent e)
    {
        Container container = modulepanel.getParent();
        container.remove(modulepanel);
        container.validate();
        container.repaint();
    }
}