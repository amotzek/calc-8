package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.Timeline;
import calc.model.TimelineListener;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm 13.02.13 22:44
 */
public final class Undo extends AbstractMediatorAction implements TimelineListener
{
    public Undo(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("UNDO") + "   ");
        putValue(ACCELERATOR_KEY, createAccelerator(KeyEvent.VK_Z));
        putValue(SMALL_ICON, getImageIcon("undo.png"));
        setEnabled(false);
        Timeline timeline = mediator.getSingleton(Timeline.class);
        timeline.addTimelineListener(this);
    }
    //
    public void timelineChanged(Timeline timeline)
    {
        final boolean enabled = timeline.canUndo();
        //
        invokeLater(() -> Undo.this.setEnabled(enabled));
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        Timeline timeline = mediator.getSingleton(Timeline.class);
        timeline.undo();
    }
}