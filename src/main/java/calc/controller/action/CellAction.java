package calc.controller.action;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import javax.swing.AbstractAction;
import calc.model.Cell;
import calc.model.Column;
import calc.model.Row;
import calc.model.Selection;
import calc.model.Sheet;
/*
 * Created by  andreasm 28.05.12 18:41
 */
abstract class CellAction extends AbstractAction
{
    private final Sheet sheet;
    private final Selection selection;
    //
    protected CellAction(Sheet sheet, Selection selection)
    {
        super();
        //
        this.sheet = sheet;
        this.selection = selection;
    }
    //
    protected final void moveSelectedCell(Cell sourcecell, int ncolumns, int nrows)
    {
        Column column = sourcecell.getColumn();
        Row row = sourcecell.getRow();
        int columnindex = column.getIndex() + ncolumns;
        int rowindex = row.getIndex() + nrows;
        //
        if (columnindex < 1 || rowindex < 1) return;
        //
        try
        {
            Cell destinationcell = sheet.findCell(columnindex, rowindex);
            selection.selectCell(destinationcell);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            //
        }
    }
}