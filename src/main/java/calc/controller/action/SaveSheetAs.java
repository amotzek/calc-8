package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.controller.command.WriteSheet;
import calc.model.FileReference;
/*
 * Created by andreasm 13.02.13 22:34
 */
public final class SaveSheetAs extends FileAction
{
    public SaveSheetAs(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("SAVE_AS") + "...   ");
    }
    //
    @Override
    protected Runnable getRunnable()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        File file = chooseFile(true, bundle.getString("SAVE"));
        //
        if (file == null) return null;
        //
        FileReference filereference = mediator.getSingleton(FileReference.class);
        filereference.setFile(file);
        //
        return mediator.createAndPostConstructObject(WriteSheet.class, file);
    }
    //
    public void run()
    {
        assert false;
    }
}