package calc.controller.action;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.controller.command.ReadModule;
import calc.view.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
/**
 * Created by andreasm on 05.06.15
 */
public final class AddModule extends ConcurrentAction
{
    public AddModule(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("ADD_MODULE"));
    }
    //
    @Override
    protected Runnable getRunnable()
    {
        ModuleSearchDialog dialog = mediator.createAndPostConstructObject(ModuleSearchDialog.class);
        dialog.setVisible(true);
        Object value = dialog.getValue();
        //
        if (dialog.isOK(value))
        {
            try
            {
                URI uri = dialog.getURI();
                //
                return mediator.createAndPostConstructObject(ReadModule.class, uri);
            }
            catch (URISyntaxException e)
            {
                assert false;
            }
        }
        //
        return null;
    }
    //
    public void run()
    {
        assert false;
    }
}