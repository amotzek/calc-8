package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import javax.swing.JFileChooser;
import calc.Mediator;
import calc.model.FileReference;
import calc.view.AppFrame;
/*
 * Created by andreasm 13.02.13 22:18
 */
abstract class FileAction extends SheetAction
{
    protected FileAction(Mediator mediator)
    {
        super(mediator);
    }
    //
    protected final File chooseFile(boolean save, String approve)
    {
        FileReference filereference = mediator.getSingleton(FileReference.class);
        AppFrame appframe = mediator.getSingleton(AppFrame.class);
        SheetFileFilter filefilter = mediator.getSingleton(SheetFileFilter.class);
        JFileChooser filechooser = new JFileChooser();
        filechooser.setDialogType(save ? JFileChooser.SAVE_DIALOG : JFileChooser.OPEN_DIALOG);
        filechooser.setFileFilter(filefilter);
        File file = filereference.getFile();
        filechooser.setCurrentDirectory(file);
        //
        int option = filechooser.showDialog(appframe, approve);
        //
        if (JFileChooser.APPROVE_OPTION != option) return null;
        //
        if (save) return filefilter.normalize(filechooser.getSelectedFile());
        //
        return filechooser.getSelectedFile();
    }
}