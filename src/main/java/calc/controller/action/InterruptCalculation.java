package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.ResourceBundle;
import calc.Mediator;
import calc.controller.Evaluator;
import calc.model.Calculation;
import calc.model.CalculationListener;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm
 * Date: 12.12.13
 * Time: 07:45
 */
public class InterruptCalculation extends ConcurrentAction implements CalculationListener
{
    public InterruptCalculation(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        Calculation calculation = mediator.getSingleton(Calculation.class);
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("INTERRUPT_CALCULATION") + "   ");
        setEnabled();
        calculation.addCalculationListener(this);
    }
    //
    public void calculationStateChanged()
    {
        setEnabled();
    }
    //
    public void run()
    {
        Evaluator evaluator = mediator.getSingleton(Evaluator.class);
        evaluator.interrupt();
    }
    //
    private void setEnabled()
    {
        Calculation calculation = mediator.getSingleton(Calculation.class);
        final boolean enabled = calculation.isRunning();
        //
        invokeLater(() -> InterruptCalculation.this.setEnabled(enabled));
    }
}