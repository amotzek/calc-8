package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.Sheet;
import static javax.swing.KeyStroke.getKeyStroke;
/*
 * Created by andreasm
 * Date: 12.12.13
 * Time: 17:13
 */
public class CalculateSheet extends ConcurrentAction
{
    public CalculateSheet(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("CALCULATE_SHEET") + "   ");
        putValue(ACCELERATOR_KEY, getKeyStroke(KeyEvent.VK_F5, 0));
    }
    //
    public void run()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        sheet.reevaluate();
    }
}