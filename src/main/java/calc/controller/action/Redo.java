package calc.controller.action;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import calc.Mediator;
import calc.model.Timeline;
import calc.model.TimelineListener;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm 29.11.13 15:53
 */
public final class Redo extends AbstractMediatorAction implements TimelineListener
{
    public Redo(Mediator mediator)
    {
        super(mediator);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        Timeline timeline = mediator.getSingleton(Timeline.class);
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        putValue(NAME, bundle.getString("REDO") + "   ");
        setEnabled(false);
        timeline.addTimelineListener(this);
    }
    //
    public void timelineChanged(Timeline timeline)
    {
        final boolean enabled = timeline.canRedo();
        //
        invokeLater(() -> Redo.this.setEnabled(enabled));
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        Timeline timeline = mediator.getSingleton(Timeline.class);
        timeline.redo();
    }
}