package calc.controller;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
/*
 * Created by  andreasm 02.10.12 20:31
 */
public class SexpressionTextAreaController<T extends JTextArea> extends MediatorController implements CaretListener
{
    private static final Highlighter.HighlightPainter PAINTER = new DefaultHighlighter.DefaultHighlightPainter(Color.LIGHT_GRAY);
    //
    private final Class<T> clazz;
    private volatile Object tag;
    //
    public SexpressionTextAreaController(Mediator mediator, Class<T> clazz)
    {
        super(mediator);
        //
        this.clazz = clazz;
    }
    //
    public void postConstruct()
    {
        JTextArea textarea = mediator.getSingleton(clazz);
        textarea.addCaretListener(this);
    }
    //
    public final void caretUpdate(CaretEvent event)
    {
        JTextArea textarea = mediator.getSingleton(clazz);
        Document document = textarea.getDocument();
        Highlighter highlighter = textarea.getHighlighter();
        //
        try
        {
            int dot = event.getDot();
            String text = document.getText(0, dot);
            //
            if (text.endsWith(")"))
            {
                int open = openingBracket(text);
                //
                if (open >= 0)
                {
                    if (tag != null) highlighter.removeHighlight(tag);
                    //
                    tag = highlighter.addHighlight(open, open + 1, PAINTER);
                    //
                    return;
                }
            }
            //
            if (tag != null)
            {
                highlighter.removeHighlight(tag);
                tag = null;
            }
        }
        catch (BadLocationException e)
        {
            // no highlighting
        }
    }
    //
    private static int openingBracket(String text)
    {
        int match = 0;
        //
        for (int index = text.length() - 1; index >= 0; index--)
        {
            char c = text.charAt(index);
            //
            if (c == ')')
            {
                match++;
            }
            else if (c == '(')
            {
                match--;
            }
            //
            if (match == 0) return index;
        }
        //
        return -1;
    }
}