package calc.controller;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import calc.Mediator;
import calc.controller.action.MagnifyCell;
import calc.model.Cell;
import calc.model.Selection;
import calc.view.SheetPanel;
/*
 * Created by  andreasm 20.05.12 11:35
 */
@SuppressWarnings("unused")
public final class SheetController extends MediatorController implements MouseListener
{
    public SheetController(Mediator mediator)
    {
        super(mediator);
    }
    //
    public void postConstruct()
    {
        SheetPanel sheetpanel = mediator.getSingleton(SheetPanel.class);
        sheetpanel.addMouseListener(this);
    }
    //
    public void mouseClicked(MouseEvent event)
    {
        Selection selection = mediator.getSingleton(Selection.class);
        SheetPanel sheetpanel = mediator.getSingleton(SheetPanel.class);
        Point point = event.getPoint();
        Cell cell = sheetpanel.findCell(point);
        //
        if (cell == null) return;
        //
        int clicks = event.getClickCount();
        //
        switch (clicks)
        {
            case 1:
                if (event.isShiftDown())
                {
                    try
                    {
                        selection.selectUntilCell(cell);
                    }
                    catch (IllegalStateException e)
                    {
                        // ignore
                    }
                }
                else
                {
                    selection.selectCell(cell);
                }
                //
                break;
            //
            case 2:
                selection.selectCell(cell);
                MagnifyCell magnifycell = mediator.getSingleton(MagnifyCell.class);
                magnifycell.actionPerformed(null);
                //
                break;
        }
    }
    //
    public void mousePressed(MouseEvent event)
    {
    }
    //
    public void mouseReleased(MouseEvent event)
    {
    }
    //
    public void mouseEntered(MouseEvent event)
    {
    }
    //
    public void mouseExited(MouseEvent event)
    {
    }
}