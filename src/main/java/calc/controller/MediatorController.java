package calc.controller;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
/*
 * Created by andreasm on 22.09.15.
 */
abstract class MediatorController
{
    protected final Mediator mediator;
    //
    protected MediatorController(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
}