package calc.controller;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.text.Caret;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import calc.Mediator;
import calc.controller.command.CellFormula;
import calc.model.Cell;
import calc.model.Selection;
import calc.model.Timeline;
import calc.view.FormulaTextArea;
/*
 * Created by andreasm 02.12.13 07:54
 */
@SuppressWarnings("unused")
public final class FormulaTextAreaController extends SexpressionTextAreaController<FormulaTextArea> implements KeyListener, FocusListener
{
    private static final long DELAY = 500L;
    //
    private volatile Cell lastcell;
    //
    public FormulaTextAreaController(Mediator mediator)
    {
        super(mediator, FormulaTextArea.class);
    }
    //
    public void postConstruct()
    {
        super.postConstruct();
        FormulaTextArea textarea = mediator.getSingleton(FormulaTextArea.class);
        textarea.addKeyListener(this);
        textarea.addFocusListener(this);
    }
    //
    public void keyTyped(KeyEvent event)
    {
    }
    //
    public void keyPressed(KeyEvent event)
    {
    }
    //
    public void keyReleased(KeyEvent event)
    {
        Selection selection = mediator.getSingleton(Selection.class);
        Timeline timeline = mediator.getSingleton(Timeline.class);
        FormulaTextArea textarea = mediator.getSingleton(FormulaTextArea.class);
        Cell cell = textarea.getCell();
        //
        if (cell == null) return;
        /*
         * must be processed in keyReleased
         * because otherwise the text in
         * the text area will not have changed
         */
        long now = event.getWhen();
        long lastupdate = cell.getLastUpdate();
        String oldformula = cell.getFormula();
        String newformula = textarea.getText();
        //
        if (cell.setFormula(newformula, now) && (cellChanged(cell) || oldEnough(now, lastupdate)))
        {
            UndoableEdit undokey = new UndoKey(selection, cell, lastupdate, now, oldformula, newformula);
            timeline.addEdit(undokey);
        }
    }
    //
    public void focusGained(FocusEvent event)
    {
        FormulaTextArea textarea = mediator.getSingleton(FormulaTextArea.class);
        //
        if (!textarea.isEditable())
        {
            Caret caret = textarea.getCaret();
            caret.setVisible(true);
        }
    }
    //
    public void focusLost(FocusEvent e)
    {
    }
    //
    private boolean cellChanged(Cell cell)
    {
        if (lastcell == null)
        {
            lastcell = cell;
            //
            return false;
        }
        //
        if (cell == lastcell) return false;
        //
        lastcell = cell;
        //
        return true;
    }
    //
    private static boolean oldEnough(long now, long past)
    {
        return now - past > DELAY;
    }
    //
    private static class UndoKey extends AbstractUndoableEdit
    {
        private final Selection selection;
        private final CellFormula undocellformula;
        private final CellFormula redocellformula;
        //
        public UndoKey(Selection selection, Cell cell, long lastupdate, long now, String oldformula, String newformula)
        {
            super();
            //
            this.selection = selection;
            //
            undocellformula = new CellFormula(cell, oldformula, lastupdate);
            redocellformula = new CellFormula(cell, newformula, now);
        }
        //
        @Override
        public void undo() throws CannotUndoException
        {
            super.undo();
            //
            setCell(undocellformula);
        }
        //
        @Override
        public void redo() throws CannotRedoException
        {
            super.redo();
            //
            setCell(redocellformula);
        }
        //
        private void setCell(CellFormula cellformula)
        {
            Cell selectedcell = selection.getSelectedCell();
            //
            if (selectedcell != null) selection.clear();
            //
            Cell cell = cellformula.getCell();
            String formula = cellformula.getFormula();
            long lastupdate = cellformula.getLastupdate();
            cell.setFormula(formula, lastupdate);
            //
            if (selectedcell != null) selection.selectCell(selectedcell);
        }
    }
}