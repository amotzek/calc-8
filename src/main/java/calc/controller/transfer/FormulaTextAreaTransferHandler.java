package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import calc.Mediator;
import calc.model.Cell;
import calc.model.Column;
import calc.model.Row;
import calc.model.Selection;
import calc.model.Sheet;
import calc.model.Timeline;
import calc.view.FormulaTextArea;
/*
 * Created by andreasm 23.01.13 07:22
 */
public final class FormulaTextAreaTransferHandler extends CellBlockTransferHandler
{
    private final Mediator mediator;
    private final TransferHandler delegate;
    //
    public FormulaTextAreaTransferHandler(Mediator mediator, TransferHandler delegate)
    {
        super();
        //
        this.mediator = mediator;
        this.delegate = delegate;
    }
    //
    @Override
    public Transferable createTransferable(JComponent component)
    {
        FormulaTextArea textarea = (FormulaTextArea) component;
        Cell cell = textarea.getCell();
        String text = textarea.getSelectedText();
        //
        if (cell == null || text == null || text.isEmpty()) return null;
        //
        Row row = cell.getRow();
        Column column = cell.getColumn();
        int rowindex = row.getIndex();
        int columnindex = column.getIndex();
        CellSnippet cellsnippet = new CellSnippet(rowindex, columnindex, text);
        //
        return new CellSnippetTransferable(cellsnippet);
    }
    //
    @Override
    public boolean canImport(JComponent component, DataFlavor[] flavors)
    {
        if (component instanceof FormulaTextArea && component.isEnabled())
        {
            FormulaTextArea textarea = (FormulaTextArea) component;
            //
            for (DataFlavor flavor : flavors)
            {
                if (isCellSnippetFlavor(flavor)) return true;
                //
                if (isCellBlockFlavor(flavor) && isAllOrNothingSelected(textarea)) return true;
            }
        }
        //
        return delegate.canImport(component, flavors);
    }
    //
    private static boolean isAllOrNothingSelected(FormulaTextArea textarea)
    {
        int start = textarea.getSelectionStart();
        int end = textarea.getSelectionEnd();
        //
        if (start == 0 && end == 0) return true;
        //
        String text = textarea.getText();
        int length = text.length();
        //
        return start == 0 && end == length;
    }
    //
    @Override
    public boolean importData(JComponent component, Transferable transferable)
    {
        DataFlavor[] flavors = transferable.getTransferDataFlavors();
        //
        if (!canImport(component, flavors)) return false;
        //
        try
        {
            if (component instanceof FormulaTextArea)
            {
                FormulaTextArea textarea = (FormulaTextArea) component;
                //
                if (transferable.isDataFlavorSupported(CellSnippet.flavor))
                {
                    CellSnippet cellsnippet = (CellSnippet) transferable.getTransferData(CellSnippet.flavor);
                    importSnippet(cellsnippet, textarea);
                    //
                    return true;
                }
                //
                if (transferable.isDataFlavorSupported(CellBlock.flavor))
                {
                    Sheet sheet = mediator.getSingleton(Sheet.class);
                    Selection selection = mediator.getSingleton(Selection.class);
                    Timeline timeline = mediator.getSingleton(Timeline.class);
                    Cell cell = selection.getSelectedCell();
                    //
                    if (cell == null) return false;
                    //
                    Row row = cell.getRow();
                    Column column = cell.getColumn();
                    int torowindex = row.getIndex();
                    int tocolumnindex = column.getIndex();
                    CellBlock block = (CellBlock) transferable.getTransferData(CellBlock.flavor);
                    importBlock(block, torowindex, tocolumnindex, sheet, selection, timeline);
                    //
                    return true;
                }
            }
        }
        catch (CannotTranslateException e)
        {
            return false;
        }
        catch (UnsupportedFlavorException | IOException e)
        {
            // delegate
        }
        //
        return delegate.importData(component, transferable);
    }
    //
    private static void importSnippet(CellSnippet cellsnippet, FormulaTextArea textarea) throws CannotTranslateException
    {
        Cell tocell = textarea.getCell();
        String snippet = cellsnippet.getSnippet();
        int fromrowidex = cellsnippet.getRowIndex();
        int fromcolumnindex = cellsnippet.getColumnIndex();
        Translation translation = translationfactory.createTranslation(fromrowidex, fromcolumnindex, tocell);
        String translatedsnippet = translation.translate(snippet);
        textarea.replaceSelection(translatedsnippet);
    }
    //
    @Override
    protected void exportDone(JComponent component, Transferable transferable, int action)
    {
        if (action == MOVE)
        {
            FormulaTextArea textarea = (FormulaTextArea) component;
            textarea.replaceSelection("");
        }
    }
    //
    @Override
    public Icon getVisualRepresentation(Transferable transferable)
    {
        return delegate.getVisualRepresentation(transferable);
    }
}