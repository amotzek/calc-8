package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.datatransfer.DataFlavor;
import java.util.LinkedList;
import javax.swing.*;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import calc.controller.command.CellFormula;
import calc.model.Cell;
import calc.model.Selection;
import calc.model.Sheet;
import calc.model.Timeline;
/*
 * Created by andreasm@qrst.de 09.12.13 08:12
 */
abstract class CellBlockTransferHandler extends TransferHandler
{
    protected static final TranslationFactory translationfactory = new TranslationFactory();
    //
    @Override
    public final int getSourceActions(JComponent component)
    {
        return COPY_OR_MOVE;
    }
    //
    protected static void importBlock(CellBlock block, int torowindex, int tocolumnindex, Sheet sheet, Selection selection, Timeline timeline) throws CannotTranslateException
    {
        int fromrowindex = block.getRowIndex();
        int fromcolumnindex = block.getColumnIndex();
        int rowcount = block.getRowCount();
        int columncount = block.getColumnCount();
        String[][] formulas = block.getFormulas();
        long now = System.currentTimeMillis();
        CellBlockEdit edit = new CellBlockEdit(selection);
        //
        for (int i = 0; i < columncount; i++)
        {
            for (int j = 0; j < rowcount; j++)
            {
                String formula = formulas[i][j];
                Cell cell = sheet.findCell(tocolumnindex + i, torowindex + j);
                Translation translation = translationfactory.createTranslation(fromrowindex + j, fromcolumnindex + i, cell);
                String translatedformula = translation.translate(formula);
                String oldformula = cell.getFormula();
                long oldlastupdate = cell.getLastUpdate();
                edit.addCellFormulas(cell, translatedformula, now, oldformula, oldlastupdate);
            }
        }
        //
        edit.execute();
        timeline.addEdit(edit);
    }
    //
    protected static void clearBlock(CellBlock block, Sheet sheet, Selection selection, Timeline timeline)
    {
        int rowindex = block.getRowIndex();
        int columnindex = block.getColumnIndex();
        int rowcount = block.getRowCount();
        int columncount = block.getColumnCount();
        long now = System.currentTimeMillis();
        CellBlockEdit edit = new CellBlockEdit(selection);
        //
        for (int i = 0; i < columncount; i++)
        {
            for (int j = 0; j < rowcount; j++)
            {
                Cell cell = sheet.findCell(columnindex + i, rowindex + j);
                String oldformula = cell.getFormula();
                long oldlastupdate = cell.getLastUpdate();
                edit.addCellFormulas(cell, "", now, oldformula, oldlastupdate);
            }
        }
        //
        edit.execute();
        timeline.addEdit(edit);
    }
    //
    protected static boolean isCellSnippetFlavor(DataFlavor flavor)
    {
        return CellSnippet.flavor.equals(flavor);
    }
    //
    protected static boolean isCellBlockFlavor(DataFlavor flavor)
    {
        return CellBlock.flavor.equals(flavor);
    }
    //
    protected static boolean isStringFlavor(DataFlavor flavor)
    {
        return flavor.equals(DataFlavor.stringFlavor);
    }
    //
    private static class CellBlockEdit extends AbstractUndoableEdit
    {
        private final Selection selection;
        private final LinkedList<CellFormula> undocellformulas;
        private final LinkedList<CellFormula> redocellformulas;
        //
        protected CellBlockEdit(Selection selection)
        {
            super();
            //
            this.selection = selection;
            //
            undocellformulas = new LinkedList<>();
            redocellformulas = new LinkedList<>();
        }
        //
        protected void addCellFormulas(Cell cell, String newformula, long newlastupdate, String oldformula, long oldlastupdate)
        {
            undocellformulas.add(new CellFormula(cell, oldformula, oldlastupdate));
            redocellformulas.add(new CellFormula(cell, newformula, newlastupdate));
        }
        //
        @Override
        public void undo() throws CannotUndoException
        {
            super.undo();
            //
            setCells(undocellformulas);
        }
        //
        @Override
        public void redo() throws CannotRedoException
        {
            super.redo();
            //
            setCells(redocellformulas);
        }
        //
        public void execute()
        {
            setCells(redocellformulas);
        }
        //
        private void setCells(LinkedList<CellFormula> cellformulas)
        {
            Cell selectedcell = selection.getSelectedCell();
            //
            if (selectedcell != null) selection.clear();
            //
            for (CellFormula cellformula : cellformulas)
            {
                Cell cell = cellformula.getCell();
                String formula = cellformula.getFormula();
                long lastupdate = cellformula.getLastupdate();
                cell.setFormula(formula, lastupdate);
            }
            //
            if (selectedcell != null) selection.selectCell(selectedcell);
        }
    }
}