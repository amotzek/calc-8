package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Cell;
import calc.model.Column;
import calc.model.Row;
/*
 * Created by  andreasm 23.01.13 08:46
 */
final class TranslationFactory
{
    private final Translation notranslation;
    //
    public TranslationFactory()
    {
        super();
        //
        notranslation = new Translation(0, 0);
    }
    //
    public Translation createTranslation(int fromrowidex, int fromcolumnindex, Cell cell)
    {
        if (cell == null) return notranslation;
        //
        Row row = cell.getRow();
        Column column = cell.getColumn();
        int torowindex = row.getIndex();
        int tocolumnindex = column.getIndex();
        int rows = torowindex - fromrowidex;
        int columns = tocolumnindex - fromcolumnindex;
        //
        if (rows == 0 && columns == 0) return notranslation;
        //
        return new Translation(rows, columns);
    }
}