package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
/*
 * Created by andreasm 01.12.13 12:04
 */
public class CellBlockTransferable implements Transferable
{
    private final CellBlock cellblock;
    private final String values;
    //
    public CellBlockTransferable(CellBlock cellblock, String values)
    {
        super();
        //
        this.cellblock = cellblock;
        this.values = values;
    }
    //
    public DataFlavor[] getTransferDataFlavors()
    {
        return new DataFlavor[] { CellBlock.flavor, DataFlavor.stringFlavor };
    }
    //
    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
        return flavor.equals(CellBlock.flavor) || flavor.equals(DataFlavor.stringFlavor);
    }
    //
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
    {
        if (flavor.equals(CellBlock.flavor)) return cellblock;
        //
        if (flavor.equals(DataFlavor.stringFlavor)) return values;
        //
        throw new UnsupportedFlavorException(flavor);
    }
}