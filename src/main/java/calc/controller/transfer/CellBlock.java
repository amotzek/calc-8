package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.datatransfer.DataFlavor;
import java.io.Serializable;
/*
 * Created by andreasm 30.11.13 11:37
 */
final class CellBlock implements Serializable
{
    public static final DataFlavor flavor = new DataFlavor(CellBlock.class, "Block");
    //
    private final int rowindex;
    private final int columnindex;
    private final int rowcount;
    private final int columncount;
    private final String[][] formulas;
    //
    public CellBlock(int rowindex, int columnindex, int rowcount, int columncount, String[][] formulas)
    {
        super();
        //
        this.rowindex = rowindex;
        this.columnindex = columnindex;
        this.rowcount = rowcount;
        this.columncount = columncount;
        this.formulas = formulas;
    }
    //
    public int getRowIndex()
    {
        return rowindex;
    }
    //
    public int getColumnIndex()
    {
        return columnindex;
    }
    //
    public int getRowCount()
    {
        return rowcount;
    }
    //
    public int getColumnCount()
    {
        return columncount;
    }
    //
    public String[][] getFormulas()
    {
        return formulas;
    }
}