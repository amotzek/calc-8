package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import java.util.StringTokenizer;
/*
 * Created by andreasm 13.12.13 17:35
 */
final class ExcelParser
{
    private final StringTokenizer tokenizer;
    private LinkedList<LinkedList<String>> rows;
    private LinkedList<String> row;
    private String token;
    private StringBuilder formulabuilder;
    private String[][] formulas;
    //
    public ExcelParser(String string)
    {
        super();
        //
        tokenizer = new StringTokenizer(string, "\n\t\"\\", true);
    }
    //
    public CellBlock getCellBlock(int rowindex, int columnindex)
    {
        if (formulas == null)
        {
            parseRows();
            setFormulas();
        }
        //
        int columncount = formulas.length;
        int rowcount = formulas[0].length;
        //
        return new CellBlock(rowindex, columnindex, rowcount, columncount, formulas);
    }
    //
    private void parseRows()
    {
        rows = new LinkedList<>();
        //
        while (hasNextToken())
        {
            parseRow();
        }
    }
    //
    private void parseRow()
    {
        row = new LinkedList<>();
        //
        while (hasNextToken())
        {
            parseFormula();
            //
            if (isNewLine()) break;
        }
        //
        rows.addLast(row);
        row = null;
    }
    //
    private void parseFormula()
    {
        beginFormula();
        nextToken();
        //
        if (isQuote())
        {
            copyUntilQuote();
            nextToken();
            //
            while (isQuote())
            {
                appendQuote();
                copyUntilQuote();
                nextToken();
            }
        }
        else
        {
            copyUntilNewLineOrTab();
        }
        //
        row.addLast(endFormula());
    }
    //
    private void beginFormula()
    {
        formulabuilder = new StringBuilder();
    }
    //
    private void appendQuote()
    {
        formulabuilder.append('"');
    }
    //
    private void copyUntilQuote()
    {
        boolean escape = false;
        nextToken();
        //
        while (!isQuote() || escape)
        {
            if (escape)
            {
                formulabuilder.append(token);
                escape = false;
            }
            else
            {
                if (isBackslash())
                {
                    escape = true;
                }
                else
                {
                    formulabuilder.append(token);
                }
            }
            //
            nextToken();
        }
    }
    //
    private void copyUntilNewLineOrTab()
    {
        boolean escape = false;
        //
        while (!isNewLine() && !isTab())
        {
            if (escape)
            {
                formulabuilder.append(token);
                escape = false;
            }
            else
            {
                if (isBackslash())
                {
                    escape = true;
                }
                else
                {
                    formulabuilder.append(token);
                }
            }
            //
            nextToken();
        }
    }
    //
    private String endFormula()
    {
        String formula = formulabuilder.toString();
        formulabuilder = null;
        //
        return formula;
    }
    //
    private boolean isTab()
    {
        return token.equals("\t");
    }
    //
    private boolean isNewLine()
    {
        return token.equals("\n");
    }
    //
    private boolean isQuote()
    {
        return token.equals("\"");
    }
    //
    private boolean isBackslash()
    {
        return token.equals("\\");
    }
    //
    private boolean hasNextToken()
    {
        return tokenizer.hasMoreTokens();
    }
    //
    private void nextToken()
    {
        token = tokenizer.nextToken();
    }
    //
    private void setFormulas()
    {
        int columncount = 0;
        //
        for (LinkedList<String> row : rows)
        {
            int size = row.size();
            //
            if (size > columncount) columncount = size;
        }
        //
        formulas = new String[columncount][rows.size()];
        int rowindex = 0;
        //
        for (LinkedList<String> row : rows)
        {
            int columnindex = 0;
            //
            for (String formula : row)
            {
                formulas[columnindex][rowindex] = formula;
                columnindex++;
            }
            //
            rowindex++;
        }
        //
        rows.clear();
    }
}