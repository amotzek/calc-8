package calc.controller.transfer;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import calc.model.Column;
import calc.model.Row;
/*
 * Created by andreasm 23.01.13 08:43
 */
final class Translation
{
    private static final char AT = '@';
    private static final char COLON = ':';
    private static final String PIN = "!";
    //
    private static final Pattern pattern = Pattern.compile("@[a-zA-Z*]+!?[0-9]+!?(:[a-zA-Z*]+!?[0-9]+!?)?");
    //
    private final int rows;
    private final int columns;
    //
    public Translation(int rows, int columns)
    {
        super();
        //
        this.rows = rows;
        this.columns = columns;
    }
    //
    public String translate(String snippet) throws CannotTranslateException
    {
        if (rows == 0 && columns == 0) return snippet;
        //
        Matcher matcher = pattern.matcher(snippet);
        StringBuilder output = new StringBuilder();
        int position = 0;
        //
        while (matcher.find())
        {
            int start = matcher.start();
            int end = matcher.end();
            String token = snippet.substring(start, end);
            output.append(snippet.substring(position, start));
            appendTranslated(token, output);
            position = end;
        }
        //
        output.append(snippet.substring(position));
        //
        return output.toString();
    }
    //
    private void appendTranslated(String input, StringBuilder output) throws CannotTranslateException
    {
        int end = input.length();
        int colon = input.indexOf(COLON);
        output.append(AT);
        //
        if (colon > 0)
        {
            appendTranslated(input, 1, colon, output);
            output.append(COLON);
            appendTranslated(input, colon + 1, end, output);
        }
        else
        {
            appendTranslated(input, 1, end, output);
        }
    }
    //
    private void appendTranslated(String input, int position, int end, StringBuilder output) throws CannotTranslateException
    {
        try
        {
            int letter = Column.nextLetter(input, position);
            int digit = Row.nextDigit(input, letter);
            String columnlabel = input.substring(letter, digit);
            String rowlabel = input.substring(digit, end);
            //
            if (!columnlabel.endsWith(PIN))
            {
                int columnindex = Column.toIndex(columnlabel);
                columnindex += columns;
                columnlabel = Column.toLabel(columnindex).toLowerCase();
            }
            //
            if (!rowlabel.endsWith(PIN))
            {
                int rowindex = Row.toIndex(rowlabel);
                rowindex += rows;
                rowlabel = Row.toLabel(rowindex);
            }
            //
            output.append(columnlabel);
            output.append(rowlabel);
        }
        catch (IllegalArgumentException e)
        {
            throw new CannotTranslateException();
        }
    }
}