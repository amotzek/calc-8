package calc.controller;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.WizardModel;
import calc.view.AbstractWizardDialog;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Created by andreasm on 16.09.18
 */
abstract class AbstractWizardController<D extends AbstractWizardDialog> implements ActionListener
{
    private final WizardModel model;
    private final D dialog;
    //
    AbstractWizardController(WizardModel model, D dialog)
    {
        super();
        //
        this.model = model;
        this.dialog = dialog;
    }
    //
    public void connect()
    {
        JButton backbutton = dialog.getBackButton();
        JButton nextbutton = dialog.getNextButton();
        backbutton.addActionListener(this);
        nextbutton.addActionListener(this);
        backbutton.setEnabled(hasBackStep());
        nextbutton.setEnabled(hasNextStep());
    }
    //
    @Override
    public void actionPerformed(ActionEvent event)
    {
        Object source = event.getSource();
        JButton backbutton = dialog.getBackButton();
        JButton nextbutton = dialog.getNextButton();
        //
        if (source == backbutton)
        {
            String backstep = getBackStep();
            model.setStep(backstep);
            nextbutton.setEnabled(hasNextStep());
            backbutton.setEnabled(hasBackStep());
            //
            return;
        }
        //
        if (source == nextbutton)
        {
            if (!hasNextStep())
            {
                dialog.dispose();
                //
                return;
            }
            //
            model.setStep(getNextStep());
            //
            if (hasBackStep() || hasNextStep())
            {
                nextbutton.setEnabled(hasNextStep());
                backbutton.setEnabled(hasBackStep());
                //
                return;
            }
            //
            backbutton.setVisible(false);
            nextbutton.setText("   OK   ");
            nextbutton.setEnabled(true);
        }
    }
    //
    abstract String getNextStep();
    //
    abstract String getBackStep();
    //
    abstract boolean hasNextStep();
    //
    abstract boolean hasBackStep();
    //
    final String getCurrentStep()
    {
        return model.getStep();
    }
    //
    final D getDialog()
    {
        return dialog;
    }
}
