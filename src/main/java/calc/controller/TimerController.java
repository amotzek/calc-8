package calc.controller;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Timer;
import calc.Mediator;
import calc.controller.command.WriteSheet;
import calc.model.FileReference;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
/*
 * Created by andreasm
 * Date: 02.12.13
 * Time: 08:11
 */
@SuppressWarnings("unused")
public final class TimerController extends MediatorController implements ActionListener
{
    private static final int FIVE_MINS = 300000;
    //
    public TimerController(Mediator mediator)
    {
        super(mediator);
    }
    //
    public void postConstruct()
    {
        Timer timer = new Timer(FIVE_MINS, this);
        timer.setRepeats(true);
        timer.start();
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        FileReference filereference = mediator.getSingleton(FileReference.class);
        File file = filereference.getFile();
        Runnable writesheet = mediator.createAndPostConstructObject(WriteSheet.class, file);
        RunnableQueue runnablequeue = RunnableQueueFactory.getConcurrentRunnableQueue();
        runnablequeue.add(writesheet);
    }
}