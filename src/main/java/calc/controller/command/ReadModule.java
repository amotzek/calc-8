package calc.controller.command;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.model.ModuleRepository;
import calc.view.ExceptionDialogFactory;
import calc.view.ModulesPanel;
import lisp.module.Module;
import javax.swing.JComponent;
import java.net.URI;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.SwingUtilities.invokeLater;
/**
 * Created by andreasm on 07.06.15
 */
public final class ReadModule implements Runnable
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final Mediator mediator;
    private final URI uri;
    //
    public ReadModule(Mediator mediator, URI uri)
    {
        super();
        //
        this.mediator = mediator;
        this.uri = uri;
    }
    //
    public void run()
    {
        final ModulesPanel modulespanel = mediator.getSingleton(ModulesPanel.class);
        //
        try
        {
            ModuleRepository repository = new ModuleRepository();
            Collection<Module> modules = modulespanel.getModules();
            repository.addModules(modules);
            final Module module = repository.addModule(uri);
            repository.satisfyDependencies();
            //
            if (module == null) return;
            //
            invokeLater(() -> {
                JComponent modulecomponent = modulespanel.addOrUpdateModule(module);
                modulespanel.validateAndScroll(modulecomponent);
            });
        }
        catch (final Exception e)
        {
            logger.log(Level.WARNING, "cannot read module", e);
            //
            invokeLater(() -> {
                ExceptionDialogFactory dialogfactory = mediator.getSingleton(ExceptionDialogFactory.class);
                dialogfactory.showDialog("ADD_MODULE", "CANNOT_ADD_MODULE", e);
            });
        }
    }
}