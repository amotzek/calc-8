package calc.controller.command;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.io.SheetWriter;
import calc.model.Sheet;
import calc.view.ExceptionDialogFactory;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm on 24.05.12 at 09:00
 */
public final class WriteSheet implements Runnable
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final Mediator mediator;
    private final File file;
    //
    public WriteSheet(Mediator mediator, File file)
    {
        super();
        //
        this.mediator = mediator;
        this.file = file;
    }
    //
    public void run()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        //
        try
        {
            SheetWriter writer = new SheetWriter(sheet);
            writer.run();
            writer.writeTo(file);
        }
        catch (final Exception e)
        {
            logger.log(Level.WARNING, "cannot write sheet", e);
            //
            invokeLater(() -> {
                ExceptionDialogFactory dialogfactory = mediator.getSingleton(ExceptionDialogFactory.class);
                dialogfactory.showDialog("SAVE_SHEET", "CANNOT_SAVE_SHEET", e);
            });
        }
    }
}