package calc.controller.command;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.io.Session;
import calc.model.ServiceModel;
import calc.model.Sheet;
import calc.view.ExceptionDialogFactory;
import lisp.module.Module;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm on 24.09.18
 */
public final class PushModule implements Runnable
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final Mediator mediator;
    //
    public PushModule(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @Override
    public void run()
    {
        try
        {
            Sheet sheet = mediator.getSingleton(Sheet.class);
            ServiceModel servicemodel = mediator.getSingleton(ServiceModel.class);
            String name = sheet.getModuleName();
            String commentde = sheet.getModuleCommentDe();
            Collection<String> exports = sheet.getModuleExports();
            String body = sheet.getProgram();
            Collection<Module> dependencies = sheet.getModules();
            String server = servicemodel.getServer();
            String emailaddress = servicemodel.getEmailAddress();
            char[] password = servicemodel.getPassword();
            Session session = new Session(server);
            session.login(emailaddress, password);
            //
            try
            {
                session.pushModule(name, commentde, exports, body, dependencies);
            }
            finally
            {
                session.logout();
            }
        }
        catch (final Exception e)
        {
            logger.log(Level.WARNING, "cannot push module", e);
            //
            invokeLater(() ->
            {
                ExceptionDialogFactory dialogfactory = mediator.getSingleton(ExceptionDialogFactory.class);
                dialogfactory.showDialog("SAVE_MODULE", "CANNOT_SAVE_MODULE", e);
            });
        }
    }
}
