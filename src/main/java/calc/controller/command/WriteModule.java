package calc.controller.command;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.io.ModuleReader;
import calc.io.ModuleWriter;
import calc.model.Sheet;
import calc.view.ExceptionDialogFactory;
import lisp.module.Module;
import lisp.module.ModuleDependency;
import propertyset.VisitorException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.SwingUtilities.invokeLater;
/**
 * Created by andreasm on 10.06.15
 */
public final class WriteModule implements Runnable
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final Mediator mediator;
    //
    public WriteModule(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    public void run()
    {
        try
        {
            Sheet sheet = mediator.getSingleton(Sheet.class);
            String name = sheet.getModuleName();
            URI uri = getURI(name);
            int version = getVersion(uri);
            String commentde = sheet.getModuleCommentDe();
            Collection<String> exports = sheet.getModuleExports();
            String body = sheet.getProgram();
            Collection<Module> modules = sheet.getModules();
            Collection<ModuleDependency> dependencies = getDependencies(modules);
            Module module = new Module(name, version, uri, commentde, "", exports, body, dependencies);
            ModuleWriter writer = new ModuleWriter(sheet, module);
            writer.run();
            writer.writeTo(uri);
        }
        catch (final Exception e)
        {
            logger.log(Level.WARNING, "cannot write module", e);
            //
            invokeLater(() -> {
                ExceptionDialogFactory dialogfactory = mediator.getSingleton(ExceptionDialogFactory.class);
                dialogfactory.showDialog("SAVE_MODULE", "CANNOT_SAVE_MODULE", e);
            });
        }
    }
    //
    private static URI getURI(String name) throws URISyntaxException
    {
        return new URI("local", name, null);
    }
    //
    private static int getVersion(URI uri) throws VisitorException
    {
        try
        {
            ModuleReader reader = new ModuleReader();
            Module module = reader.readFrom(uri);
            int version = module.getVersion();
            //
            return version + 1;
        }
        catch (IOException e)
        {
            return 0;
        }
    }
    //
    private static Collection<ModuleDependency> getDependencies(Collection<Module> modules)
    {
        ArrayList<ModuleDependency> dependencies = new ArrayList<>(modules.size());
        //
        for (Module module : modules)
        {
            String name = module.getName();
            Integer version = module.getVersion();
            URI uri = module.getURI();
            ModuleDependency dependency = new ModuleDependency(name, version, uri, null);
            dependencies.add(dependency);
        }
        //
        return dependencies;
    }
}