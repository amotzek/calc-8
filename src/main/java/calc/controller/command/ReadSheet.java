package calc.controller.command;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import calc.Mediator;
import calc.io.SheetReader;
import calc.model.Cell;
import calc.model.FileReference;
import calc.model.Log;
import calc.model.Selection;
import calc.model.Sheet;
import calc.view.ExceptionDialogFactory;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by  andreasm 23.05.12 07:01
 */
public final class ReadSheet implements Runnable
{
    private static final Logger logger = Logger.getAnonymousLogger();
    //
    private final Mediator mediator;
    private final File file;
    //
    public ReadSheet(Mediator mediator, File file)
    {
        super();
        //
        this.mediator = mediator;
        this.file = file;
    }
    //
    public void run()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Selection selection = mediator.getSingleton(Selection.class);
        FileReference filereference = mediator.getSingleton(FileReference.class);
        Log log = mediator.getSingleton(Log.class);
        //
        try
        {
            filereference.clear();
            selection.clear();
            swingBarrier();
            SheetReader reader = new SheetReader(sheet, log);
            reader.readFrom(file);
            reader.run();
            Cell cell = sheet.findCell("A", 1);
            selection.selectCell(cell);
            filereference.setFile(file);
        }
        catch (final Exception e)
        {
            logger.log(Level.WARNING, "cannot read sheet", e);
            //
            invokeLater(() -> {
                ExceptionDialogFactory dialogfactory = mediator.getSingleton(ExceptionDialogFactory.class);
                dialogfactory.showDialog("OPEN_SHEET", "CANNOT_OPEN_SHEET", e);
            });
        }
    }
}