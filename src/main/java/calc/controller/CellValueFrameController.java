package calc.controller;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import calc.view.CellValueFrame;
/*
 * Created by  andreasm 31.05.12 20:56
 */
public final class CellValueFrameController implements WindowListener
{
    private final CellValueFrame frame;
    //
    public CellValueFrameController(CellValueFrame frame)
    {
        super();
        //
        this.frame = frame;
    }
    //
    public void connect()
    {
        frame.addWindowListener(this);
    }
    //
    public void windowOpened(WindowEvent event)
    {
    }
    //
    public void windowClosing(WindowEvent event)
    {
        frame.dispose();
    }
    //
    public void windowClosed(WindowEvent event)
    {
    }
    //
    public void windowIconified(WindowEvent event)
    {
    }
    //
    public void windowDeiconified(WindowEvent event)
    {
    }
    //
    public void windowActivated(WindowEvent event)
    {
    }
    //
    public void windowDeactivated(WindowEvent event)
    {
    }
}