package calc.controller;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.view.ModulePanel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
/**
 * Created by andreasm on 08.06.15
 */
public final class ModulePanelController implements MouseListener
{
    private final ModulePanel modulepanel;
    //
    public ModulePanelController(ModulePanel modulepanel)
    {
        super();
        //
        this.modulepanel = modulepanel;
    }
    //
    public void mouseClicked(MouseEvent event)
    {
    }
    //
    public void mousePressed(MouseEvent event)
    {
    }
    //
    public void mouseReleased(MouseEvent event)
    {
    }
    //
    public void mouseEntered(MouseEvent event)
    {
        modulepanel.setSelected(true);
    }
    //
    public void mouseExited(MouseEvent event)
    {
        Rectangle rectangle = modulepanel.getVisibleRect();
        Point point = event.getPoint();
        //
        if (rectangle.contains(point)) return;
        //
        modulepanel.setSelected(false);
    }
}