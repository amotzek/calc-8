package calc.controller;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.prefs.Preferences;
import calc.Mediator;
import calc.controller.command.ReadSheet;
import calc.controller.command.WriteSheet;
import calc.model.FileReference;
import calc.view.AppFrame;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
/*
 * User: andreasm
 * Date: 17.04.13
 * Time: 18:25
 */
@SuppressWarnings("unused")
public final class AppFrameController extends MediatorController implements WindowListener
{
    public AppFrameController(Mediator mediator)
    {
        super(mediator);
    }
    //
    public void postConstruct()
    {
        AppFrame frame = mediator.getSingleton(AppFrame.class);
        frame.addWindowListener(this);
    }
    //
    public void windowOpened(WindowEvent event)
    {
        FileReference filereference = mediator.getSingleton(FileReference.class);
        File file = restoreFile(filereference);
        Runnable readsheet = mediator.createAndPostConstructObject(ReadSheet.class, file);
        RunnableQueue runnablequeue = RunnableQueueFactory.getConcurrentRunnableQueue();
        runnablequeue.add(readsheet);
    }
    //
    public void windowClosing(WindowEvent event)
    {
        FileReference filereference = mediator.getSingleton(FileReference.class);
        AppFrame frame = mediator.getSingleton(AppFrame.class);
        frame.dispose();
        File file = storeFile(filereference);
        Runnable writesheet = mediator.createAndPostConstructObject(WriteSheet.class, file);
        writesheet.run();
    }
    //
    public void windowClosed(WindowEvent event)
    {
    }
    //
    public void windowIconified(WindowEvent event)
    {
    }
    //
    public void windowDeiconified(WindowEvent event)
    {
    }
    //
    public void windowActivated(WindowEvent event)
    {
    }
    //
    public void windowDeactivated(WindowEvent event)
    {
    }
    //
    private static File restoreFile(FileReference filereference)
    {
        Preferences app = getPreferences();
        //
        if (app == null) return null;
        //
        String pathname = app.get("pathname", null);
        //
        if (pathname == null) return null;
        //
        File file = new File(pathname);
        filereference.setFile(file);
        //
        return file;
    }
    //
    private static File storeFile(FileReference filereference)
    {
        File file = filereference.getFile();
        Preferences app = getPreferences();
        //
        if (app == null) return file;
        //
        if (file == null)
        {
            app.remove("pathname");
        }
        else
        {
            String pathname = file.getAbsolutePath();
            app.put("pathname", pathname);
        }
        //
        return file;
    }
    //
    private static Preferences getPreferences()
    {
        Preferences root = Preferences.userRoot();
        //
        if (root == null) return null;
        //
        return root.node("sheetapp");
    }
}