package calc.controller.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.model.Cell;
import calc.model.Sheet;
import lisp.CannotEvalException;
import lisp.Combinator;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created by andreasm on 18.05.12 at 20:59
 */
public final class CellValue extends Combinator
{
    private static final Symbol UNRESOLVED_DEPENDENCY = Symbol.createSymbol("unresolved-dependency");
    private static final Symbol NO_VALUE = Symbol.createSymbol("no-value");
    //
    private final Sheet sheet;
    private final Cell cell;
    //
    public CellValue(Sheet sheet, Cell cell)
    {
        super(0, 1);
        //
        this.sheet = sheet;
        this.cell = cell;
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Sexpression argument = arguments[0];
        //
        try
        {
            Cell foundcell = sheet.findCell(argument);
            cell.addDependency(foundcell);
            //
            if (foundcell.isDirty())
            {
                fail.fail(UNRESOLVED_DEPENDENCY, null);
                //
                return;
            }
            //
            if (foundcell.hasValue())
            {
                Sexpression value = foundcell.getValue();
                succeed.succeed(value);
                //
                return;
            }
            //
            fail.fail(NO_VALUE, null);
        }
        catch (CannotEvalException e)
        {
            fail.fail(e.getSymbol(), e.getSexpression());
        }
    }
}