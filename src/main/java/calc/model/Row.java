package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static calc.view.Style.*;
/*
 * Created by andreasm on 15.05.12 at 21:09
 */
public final class Row
{
    private final int index;
    //
    Row(int index)
    {
        super();
        //
        this.index = index;
    }
    //
    public int getIndex()
    {
        return index;
    }
    //
    public String getLabel()
    {
        return toLabel(index);
    }
    //
    public int getHeight()
    {
        return ROW_HEIGHT;
    }
    //
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (o == null || getClass() != o.getClass()) return false;
        //
        Row row = (Row) o;
        //
        if (index != row.index) return false;
        //
        return true;
    }
    //
    @Override
    public int hashCode()
    {
        return index;
    }
    //
    @Override
    public String toString()
    {
        return getLabel();
    }
    //
    public static String toLabel(int index)
    {
        if (index < 1) throw new IllegalArgumentException();
        //
        return Integer.toString(index);
    }
    //
    public static int toIndex(String label)
    {
        return Integer.parseInt(label);
    }
    //
    public static int nextDigit(String string, int index)
    {
        while (index < string.length())
        {
            char c = string.charAt(index);
            //
            if (c >= '0' && c <= '9') return index;
            //
            index++;
        }
        //
        throw new IllegalArgumentException();
    }
}