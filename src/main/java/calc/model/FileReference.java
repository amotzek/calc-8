package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.File;
import java.util.LinkedList;
/*
 * Created by  andreasm 02.06.12 19:31
 */
public final class FileReference
{
    private final LinkedList<FileListener> listeners;
    private File file;
    //
    public FileReference()
    {
        super();
        //
        listeners = new LinkedList<>();
    }
    //
    public synchronized void addFileListener(FileListener listener)
    {
        listeners.add(listener);
    }
    //
    private void fileChanged()
    {
        for (FileListener listener : listeners)
        {
            listener.fileChanged();
        }
    }
    //
    public synchronized File getFile()
    {
        return file;
    }
    //
    public synchronized void setFile(File file)
    {
        this.file = file;
        fileChanged();
    }
    //
    public void clear()
    {
        setFile(null);
    }
}