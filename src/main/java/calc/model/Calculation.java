package calc.model;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
/*
 * Created by andreasm on 14.10.2014.
 */
public final class Calculation
{
    private final LinkedList<CalculationListener> listeners;
    private boolean running;
    //
    public Calculation()
    {
        super();
        //
        listeners = new LinkedList<>();
    }
    //
    public synchronized void addCalculationListener(CalculationListener listener)
    {
        listeners.add(listener);
    }
    //
    private void calculationStateChanged()
    {
        for (CalculationListener listener : listeners)
        {
            listener.calculationStateChanged();
        }
    }
    //
    public synchronized boolean isRunning()
    {
        return running;
    }
    //
    public synchronized void stopped()
    {
        if (!running) return;
        //
        running = false;
        calculationStateChanged();
    }
    //
    public synchronized void started()
    {
        if (running) return;
        //
        running = true;
        calculationStateChanged();
    }
}