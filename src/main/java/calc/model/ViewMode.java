package calc.model;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
/*
 * Created by andreasm
 * Date: 22.11.13
 * Time: 07:48
 */
public final class ViewMode
{
    public static final String SHEET = "Sheet";
    public static final String PROGRAM = "Program";
    public static final String MODULES = "Modules";
    //
    private final LinkedList<ViewModeListener> listeners;
    private String modename;
    //
    public ViewMode()
    {
        super();
        //
        listeners = new LinkedList<>();
        //
        modename = SHEET;
    }
    //
    public synchronized void addViewModeListener(ViewModeListener listener)
    {
        listeners.add(listener);
    }
    //
    private void viewModeChanged()
    {
        for (ViewModeListener listener : listeners)
        {
            listener.viewModeChanged();
        }
    }
    //
    public synchronized String getModeName()
    {
        return modename;
    }
    //
    public synchronized void setSheetMode()
    {
        modename = SHEET;
        viewModeChanged();
    }
    //
    public synchronized void setProgramMode()
    {
        modename = PROGRAM;
        viewModeChanged();
    }
    //
    public synchronized void setModulesMode()
    {
        modename = MODULES;
        viewModeChanged();
    }
}