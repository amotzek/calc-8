package calc.model;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static java.lang.Math.abs;
import static java.lang.Math.min;
/*
 * Created by andreasm 30.11.13 11:41
 */
public class CellRange
{
    private final int rowindex;
    private final int columnindex;
    private final int rowcount;
    private final int columncount;
    //
    public CellRange(Cell selectedcell)
    {
        super();
        //
        Row row = selectedcell.getRow();
        Column column = selectedcell.getColumn();
        rowindex = row.getIndex();
        columnindex = column.getIndex();
        rowcount = 1;
        columncount = 1;
    }
    //
    public CellRange(Cell selectedcell1, Cell selectedcell2)
    {
        super();
        //
        Row row1 = selectedcell1.getRow();
        Column column1 = selectedcell1.getColumn();
        Row row2 = selectedcell2.getRow();
        Column column2 = selectedcell2.getColumn();
        //
        rowindex = min(row1.getIndex(), row2.getIndex());
        columnindex = min(column1.getIndex(), column2.getIndex());
        rowcount = 1 + abs(row1.getIndex() - row2.getIndex());
        columncount = 1 + abs(column1.getIndex() - column2.getIndex());
    }
    //
    public int getRowIndex()
    {
        return rowindex;
    }
    //
    public int getColumnIndex()
    {
        return columnindex;
    }
    //
    public int getRowCount()
    {
        return rowcount;
    }
    //
    public int getColumnCount()
    {
        return columncount;
    }
}