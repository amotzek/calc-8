package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.HashSet;
import lisp.Chars;
import lisp.Sexpression;
/*
 * Created by andreasm on 15.05.12 at 20:54
 */
public final class Cell
{
    private static final String EMPTY = "";
    private static final String NIL = "nil";
    //
    private final CellListenerDelegate listener;
    private final Column column;
    private final Row row;
    private final HashSet<Cell> dependson;
    private final HashSet<Cell> prerequisitefor;
    private String name;
    private long lastupdate;
    private int version;
    private String formula;
    private Sexpression sexpression;
    private boolean hasvalue;
    private boolean isdirty;
    private boolean changed;
    private boolean locked;
    private Sexpression value;
    private String valuestring;
    //
    Cell(CellListenerDelegate listener, Column column, Row row)
    {
        super();
        //
        this.listener = listener;
        this.column = column;
        this.row = row;
        //
        dependson = new HashSet<>();
        prerequisitefor = new HashSet<>();
    }
    //
    public Row getRow()
    {
        return row;
    }
    //
    public Column getColumn()
    {
        return column;
    }
    //
    public synchronized boolean isDirty()
    {
        return isdirty;
    }
    //
    public synchronized void dirty()
    {
        isdirty = true;
    }
    //
    synchronized boolean wasChanged()
    {
        return changed;
    }
    //
    synchronized void setUnchanged()
    {
        changed = false;
    }
    //
    public synchronized boolean hasValue()
    {
        return hasvalue;
    }
    //
    public synchronized Sexpression getValue()
    {
        return value;
    }
    //
    public synchronized String getValueString()
    {
        return valuestring;
    }
    //
    public boolean setValue(Sexpression value, int version)
    {
        String valuestring = toString(value);
        //
        synchronized (this)
        {
            if (this.version != version) return false;
            //
            this.value = value;
            this.valuestring = valuestring;
            //
            hasvalue = true;
            isdirty = false;
        }
        //
        listener.cellValueChanged(this);
        //
        return true;
    }
    //
    public boolean unsetValue(int version)
    {
        synchronized (this)
        {
            if (this.version != version) return false;
            //
            value = null;
            valuestring = EMPTY;
            hasvalue = false;
            isdirty = false;
        }
        //
        listener.cellValueChanged(this);
        //
        return true;
    }
    //
    public synchronized String getName()
    {
        return name;
    }
    //
    public synchronized void setName(String name)
    {
        this.name = name;
    }
    //
    public synchronized boolean isLocked()
    {
        return locked;
    }
    //
    public void setLocked(boolean locked)
    {
        synchronized (this)
        {
            if (this.locked == locked) return;
            //
            this.locked = locked;
        }
        //
        listener.cellLockChanged(this);
    }
    //
    public synchronized boolean hasFormula()
    {
        return formula != null && formula.length() > 0;
    }
    //
    public synchronized String getFormula()
    {
        return formula;
    }
    //
    public synchronized Sexpression getSexpression()
    {
        return sexpression;
    }
    //
    public synchronized int getVersion()
    {
        return version;
    }
    //
    public boolean setFormula(String formula, long lastupdate)
    {
        String trimmedformula = formula.trim();
        Compiler compiler = new Compiler(trimmedformula);
        compiler.run();
        Sexpression sexpression = compiler.getSexpression();
        //
        synchronized (this)
        {
            this.formula = trimmedformula;
            this.lastupdate = lastupdate;
            //
            if (hasSexpression(sexpression)) return false;
            //
            this.sexpression = sexpression;
            version++;
        }
        //
        reevaluate();
        //
        return true;
    }
    //
    public void reevaluate()
    {
        Cell[] cells;
        //
        synchronized (this)
        {
            cells = toArray(dependson);
            dependson.clear();
            hasvalue = false;
            value = null;
            valuestring = EMPTY;
            isdirty = true;
            changed = true;
        }
        //
        for (Cell cell : cells)
        {
            synchronized (cell)
            {
                cell.prerequisitefor.remove(this);
            }
        }
        //
        listener.cellValueChanged(this);
    }
    //
    public synchronized long getLastUpdate()
    {
        return lastupdate;
    }
    //
    private boolean hasSexpression(Sexpression othersexpression)
    {
        if (sexpression == null)
        {
            if (othersexpression == null) return true;
            //
            return false;
        }
        //
        return sexpression.equals(othersexpression);
    }
    //
    public void addDependency(Cell cell)
    {
        synchronized (this)
        {
            dependson.add(cell);
        }
        //
        synchronized (cell)
        {
            cell.prerequisitefor.add(this);
        }
    }
    //
    public synchronized Cell[] dependsOn()
    {
        return toArray(dependson);
    }
    //
    public synchronized Cell[] prerequisiteFor()
    {
        return toArray(prerequisitefor);
    }
    //
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (o == null || getClass() != o.getClass()) return false;
        //
        Cell cell = (Cell) o;
        //
        if (column != null ? !column.equals(cell.column) : cell.column != null) return false;
        //
        if (row != null ? !row.equals(cell.row) : cell.row != null) return false;
        //
        return true;
    }
    //
    @Override
    public int hashCode()
    {
        int result = row != null ? row.hashCode() : 0;
        result = 31 * result + (column != null ? column.hashCode() : 0);
        //
        return result;
    }
    //
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(column);
        builder.append(row);
        //
        return builder.toString();
    }
    //
    private static Cell[] toArray(HashSet<Cell> list)
    {
        int size = list.size();
        Cell[] array = new Cell[size];
        //
        return list.toArray(array);
    }
    //
    private static String toString(Sexpression value)
    {
        if (value == null) return NIL;
        //
        if (value instanceof Chars)
        {
            Chars charsvalue = (Chars) value;
            //
            return charsvalue.getString();
        }
        //
        return value.toString();
    }
}