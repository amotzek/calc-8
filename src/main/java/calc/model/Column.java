package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static calc.view.Style.*;
/*
 * Created by andreasm on 15.05.12 at 21:09
 */
public final class Column
{
    private final int index;
    private final String label;
    //
    Column(int index, String label)
    {
        super();
        //
        this.index = index;
        this.label = label;
    }
    //
    public int getIndex()
    {
        return index;
    }
    //
    public String getLabel()
    {
        return label;
    }
    //
    public int getWidth()
    {
        return COLUMN_WIDTH;
    }
    //
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (o == null || getClass() != o.getClass()) return false;
        //
        Column column = (Column) o;
        //
        if (label != null ? !label.equals(column.label) : column.label != null) return false;
        //
        return true;
    }
    //
    @Override
    public int hashCode()
    {
        return index;
    }
    //
    @Override
    public String toString()
    {
        return getLabel();
    }
    //
    public static String toLabel(int index)
    {
        if (index < 1) throw new IllegalArgumentException();
        //
        StringBuilder label = new StringBuilder();
        //
        while (index > 0)
        {
            int digit = index % 27;
            //
            if (digit == 0)
            {
                label.append("*");
            }
            else
            {
                label.append((char) ('A' + digit - 1));
            }
            //
            index /= 27;
        }
        //
        label.reverse();
        //
        return label.toString();
    }
    //
    public static int toIndex(String label)
    {
        int index = 0;
        //
        for (int position = 0; position < label.length(); position++)
        {
            index *= 27;
            char c = label.charAt(position);
            //
            if (c == '*') continue;
            //
            if (c >= 'a' && c <= 'z')
            {
                index += c;
                index -= 'a';
            }
            else if (c >= 'A' && c <= 'Z')
            {
                index += c;
                index -= 'A';
            }
            else
            {
                throw new IllegalArgumentException(label + " is not a valid column label");
            }
            //
            index++;
        }
        //
        return index;
    }
    //
    public static int nextLetter(String string, int index)
    {
        while (index < string.length())
        {
            char c = string.charAt(index);
            //
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '*') return index;
            //
            index++;
        }
        //
        throw new IllegalArgumentException();
    }
}