package calc.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm on 24.09.18
 */
public final class ServiceModel
{
    private String server;
    private String emailaddress;
    private char[] password;
    //
    public ServiceModel()
    {
        server = "https://simplysomethings.de";
        emailaddress = "";
        password = new char[0];
    }
    //
    public synchronized String getServer()
    {
        return server;
    }
    //
    public synchronized void setServer(String server)
    {
        this.server = server;
    }
    //
    public synchronized String getEmailAddress()
    {
        return emailaddress;
    }
    //
    public synchronized void setEmailAddress(String emailaddress)
    {
        this.emailaddress = emailaddress;
    }
    //
    public synchronized char[] getPassword()
    {
        return password;
    }
    //
    public synchronized void setPassword(char[] password)
    {
        if (password == null) throw new IllegalArgumentException("password must not be null");
        //
        this.password = password;
    }
}
