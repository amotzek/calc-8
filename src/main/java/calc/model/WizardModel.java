package calc.model;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
/**
 * Created by andreasm on 16.09.18
 */
public final class WizardModel
{
    public static final String START_STEP = "start";
    //
    private final LinkedList<WizardModelListener> listeners;
    private String step;
    //
    public WizardModel()
    {
        super();
        //
        step = START_STEP;
        listeners = new LinkedList<>();
    }
    //
    public synchronized void addWizardModelListener(WizardModelListener listener)
    {
        listeners.add(listener);
    }
    //
    public synchronized String getStep()
    {
        return step;
    }
    //
    public synchronized void setStep(String step)
    {
        this.step = step;
        //
        stepChanged();
    }
    //
    private void stepChanged()
    {
        for (WizardModelListener listener : listeners)
        {
            listener.stepChanged();
        }
    }
}
