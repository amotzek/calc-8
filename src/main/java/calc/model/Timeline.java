package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;
/*
 * Created by  andreasm 06.06.12 19:51
 */
public final class Timeline
{
    private final UndoManager manager;
    private final LinkedList<TimelineListener> listeners;
    //
    public Timeline()
    {
        super();
        //
        manager = new UndoManager();
        listeners = new LinkedList<>();
    }
    //
    public synchronized void addTimelineListener(TimelineListener listener)
    {
        listeners.add(listener);
    }
    //
    private synchronized void timelineChanged()
    {
        for (TimelineListener listener : listeners)
        {
            listener.timelineChanged(this);
        }
    }
    //
    public void addEdit(UndoableEdit edit)
    {
        boolean canundobefore = manager.canUndo();
        manager.addEdit(edit);
        //
        if (!canundobefore) timelineChanged();
    }
    //
    public void clear()
    {
        manager.discardAllEdits();
        timelineChanged();
    }
    //
    public void undo()
    {
        boolean canredobefore = manager.canRedo();
        manager.undo();
        //
        if (!manager.canUndo() || !canredobefore) timelineChanged();
    }
    //
    public void redo()
    {
        boolean canundobefore = manager.canUndo();
        manager.redo();
        //
        if (!manager.canRedo() || !canundobefore) timelineChanged();
    }
    //
    public boolean canUndo()
    {
        return manager.canUndo();
    }
    //
    public boolean canRedo()
    {
        return manager.canRedo();
    }
}