package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
/*
 * Created by andreasm on 21.05.12 at 11:47
 */
public final class Selection
{
    private final LinkedList<CellSelectionListener> listeners;
    private Cell selectedcell1;
    private Cell selectedcell2;
    //
    public Selection()
    {
        super();
        //
        listeners = new LinkedList<>();
    }
    //
    public synchronized void addCellSelectionListener(CellSelectionListener listener)
    {
        listeners.addLast(listener);
    }
    //
    public synchronized Cell getSelectedCell()
    {
        if (selectedcell2 == null) return selectedcell1;
        //
        return null;
    }
    //
    public synchronized CellRange getSelectedCellRange()
    {
        if (selectedcell1 == null) return null;
        //
        if (selectedcell2 == null) return new CellRange(selectedcell1);
        //
        return new CellRange(selectedcell1, selectedcell2);
    }
    //
    public synchronized boolean isColumnSelected(Column column)
    {
        if (selectedcell1 == null) return false;
        //
        if (selectedcell2 == null) return selectedcell1.getColumn() == column;
        //
        return between(selectedcell1.getColumn(), column, selectedcell2.getColumn());
    }
    //
    public synchronized boolean isRowSelected(Row row)
    {
        if (selectedcell1 == null) return false;
        //
        if (selectedcell2 == null) return selectedcell1.getRow() == row;
        //
        return between(selectedcell1.getRow(), row, selectedcell2.getRow());
    }
    //
    public void clear()
    {
        selectCell(null);
    }
    //
    public synchronized void selectCell(Cell nextcell)
    {
        if (selectedcell1 == nextcell && selectedcell2 == null) return;
        //
        Cell previouscell1 = selectedcell1;
        Cell previouscell2 = selectedcell2;
        selectedcell1 = nextcell;
        selectedcell2 = null;
        //
        if (previouscell1 != null && previouscell2 == null)
        {
            for (CellSelectionListener listener : listeners)
            {
                listener.cellUnselected(previouscell1);
            }
        }
        //
        if (nextcell != null)
        {
            boolean toporleft = isTopOrLeftOf(previouscell1, nextcell);
            //
            for (CellSelectionListener listener : listeners)
            {
                listener.cellSelected(nextcell, toporleft);
            }
        }
    }
    //
    public synchronized void selectUntilCell(Cell nextcell)
    {
        if (nextcell == null) throw new IllegalArgumentException();
        //
        if (selectedcell1 == null) throw new IllegalStateException();
        //
        if (selectedcell2 == nextcell) return;
        //
        Cell previouscell1 = selectedcell1;
        Cell previouscell2 = selectedcell2;
        //
        if (selectedcell1 == nextcell)
        {
            selectedcell2 = null;
        }
        else
        {
            selectedcell2 = nextcell;
        }
        //
        assert previouscell1 != null;
        //
        if (previouscell2 == null && selectedcell2 != null)
        {
            for (CellSelectionListener listener : listeners)
            {
                listener.cellUnselected(previouscell1);
            }
        }
        //
        if (previouscell1 == nextcell)
        {
            for (CellSelectionListener listener : listeners)
            {
                listener.cellSelected(nextcell, false);
            }
        }
        //
        if (selectedcell1 != null && selectedcell2 != null)
        {
            for (CellSelectionListener listener : listeners)
            {
                listener.cellRangeChanged(selectedcell1, selectedcell2);
            }
        }
    }
    //
    private static boolean between(Column column1, Column column2, Column column3)
    {
        int index1 = column1.getIndex();
        int index2 = column2.getIndex();
        int index3 = column3.getIndex();
        //
        if (index1 < index3) return index1 <= index2 && index2 <= index3;
        //
        return index3 <= index2 && index2 <= index1;
    }
    //
    private static boolean between(Row row1, Row row2, Row row3)
    {
        int index1 = row1.getIndex();
        int index2 = row2.getIndex();
        int index3 = row3.getIndex();
        //
        if (index1 < index3) return index1 <= index2 && index2 <= index3;
        //
        return index3 <= index2 && index2 <= index1;
    }
    //
    private static boolean isTopOrLeftOf(Cell cell1, Cell cell2)
    {
        if (cell1 == null || cell2 == null) return false;
        //
        Column column1 = cell1.getColumn();
        Column column2 = cell2.getColumn();
        Row row1 = cell1.getRow();
        Row row2 = cell2.getRow();
        //
        if (column1.getIndex() < column2.getIndex()) return true;
        //
        if (row1.getIndex() < row2.getIndex()) return true;
        //
        return false;
    }
}