package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.parser.Parser;
/*
 * Created by  andreasm 22.05.12 06:53
 */
final class Compiler
{
    private static final Symbol QUOTE = Symbol.createSymbol("quote");
    private static final Symbol CELL_VALUES = Symbol.createSymbol("cell-values");
    private static final Symbol CELL_VALUE = Symbol.createSymbol("cell-value");
    private static final char PIN = '!';
    //
    private final String formula;
    private Sexpression sexpression;
    //
    public Compiler(String formula)
    {
        super();
        //
        this.formula = formula;
    }
    //
    public void run()
    {
        Parser parser = new Parser(formula, 0);
        sexpression = parser.getSexpression();
        sexpression = compile(sexpression);
    }
    //
    public Sexpression getSexpression()
    {
        return sexpression;
    }
    //
    private static Sexpression compile(Sexpression sexpression)
    {
        if (sexpression instanceof Symbol)
        {
            Symbol symbol = (Symbol) sexpression;
            //
            return compileSymbol(symbol);
        }
        //
        if (sexpression instanceof List)
        {
            List list = (List) sexpression;
            //
            return compileList(list);
        }
        //
        return sexpression;
    }
    //
    private static Sexpression compileSymbol(Symbol symbol)
    {
        String name = symbol.getName();
        //
        if (name.startsWith("@"))
        {
            try
            {
                int colon = name.indexOf(':');
                //
                if (colon > 0)
                {
                    return cellValues(name, colon);
                }
                else
                {
                    return cellValue(name);
                }
            }
            catch (IllegalArgumentException e)
            {
                // is a normal symbol
            }
        }
        //
        return symbol;
    }
    //
    private static List compileList(List list)
    {
        List translatedlist = null;
        //
        while (list != null)
        {
            Sexpression first = list.first();
            Sexpression translatedfirst = compile(first);
            translatedlist = new List(translatedfirst, translatedlist);
            list = list.rest();
        }
        //
        return List.reverse(translatedlist);
    }
    //
    private static List cellValues(String string, int colon)
    {
        int letter1 = Column.nextLetter(string, 1);
        int digit1 = Row.nextDigit(string, letter1);
        int letter2 = Column.nextLetter(string, colon);
        int digit2 = Row.nextDigit(string, letter2);
        int end = string.length();
        Symbol column1 = column(string, letter1, digit1);
        Rational row1 = row(string, digit1, colon);
        Symbol column2 = column(string, letter2, digit2);
        Rational row2 = row(string, digit2, end);
        List list = List.list(column1, row1, column2, row2);
        list = List.list(QUOTE, list);
        list = List.list(CELL_VALUES, list);
        //
        return list;
    }
    //
    private static List cellValue(String string)
    {
        int letter = Column.nextLetter(string, 1);
        int digit = Row.nextDigit(string, letter);
        int end = string.length();
        Symbol column = column(string, letter, digit);
        Rational row = row(string, digit, end);
        List list = List.list(column, row);
        list = List.list(QUOTE, list);
        list = List.list(CELL_VALUE, list);
        //
        return list;
    }
    //
    private static Symbol column(String string, int begin, int end)
    {
        if (string.charAt(end - 1) == PIN) end--;
        //
        return Symbol.createSymbol(string.substring(begin, end));
    }
    //
    private static Rational row(String string, int begin, int end)
    {
        if (string.charAt(end - 1) == PIN) end--;
        //
        return new Rational(string.substring(begin, end));
    }
}