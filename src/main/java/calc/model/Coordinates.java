package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by  andreasm 19.05.12 15:34
 */
final class Coordinates
{
    private final Row row;
    private final Column column;
    //
    public Coordinates(Row row, Column column)
    {
        super();
        //
        this.row = row;
        this.column = column;
    }
    //
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (o == null || getClass() != o.getClass()) return false;
        //
        Coordinates that = (Coordinates) o;
        //
        if (column != null ? !column.equals(that.column) : that.column != null) return false;
        //
        if (row != null ? !row.equals(that.row) : that.row != null) return false;
        //
        return true;
    }
    //
    @Override
    public int hashCode()
    {
        int result = row != null ? row.hashCode() : 0;
        result = 257 * result + (column != null ? column.hashCode() : 0);
        //
        return result;
    }
}