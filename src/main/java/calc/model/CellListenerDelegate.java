package calc.model;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
/*
 * Created by  andreasm 31.05.12 21:20
 */
final class CellListenerDelegate implements CellValueListener, CellLockListener
{
    private final LinkedList<CellValueListener> valuelisteners;
    private final LinkedList<CellLockListener> locklisteners;
    //
    public CellListenerDelegate()
    {
        super();
        //
        valuelisteners = new LinkedList<>();
        locklisteners = new LinkedList<>();
    }
    //
    public synchronized void addCellValueListener(CellValueListener listener)
    {
        valuelisteners.addFirst(listener);
    }
    //
    public synchronized void removeCellValueListener(CellValueListener listener)
    {
        valuelisteners.remove(listener);
    }
    //
    public synchronized void addCellLockListener(CellLockListener listener)
    {
        locklisteners.addFirst(listener);
    }
    //
    public synchronized void cellValueChanged(Cell cell)
    {
        for (CellValueListener valuelistener : valuelisteners)
        {
            valuelistener.cellValueChanged(cell);
        }
    }
    //
    public synchronized void cellLockChanged(Cell cell)
    {
        for (CellLockListener locklistener : locklisteners)
        {
            locklistener.cellLockChanged(cell);
        }
    }
}