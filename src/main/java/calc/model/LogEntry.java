package calc.model;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.Symbol;
/*
 * Created by  andreasm 20.01.13 10:24
 */
public final class LogEntry
{
    private final Symbol symbol;
    private final Sexpression sexpression;
    private final Row row;
    private final Column column;
    //
    LogEntry(CannotEvalException e, Row row, Column column)
    {
        super();
        //
        this.symbol = e.getSymbol();
        this.sexpression = e.getSexpression();
        this.row = row;
        this.column = column;
    }
    //
    public Symbol getSymbol()
    {
        return symbol;
    }
    //
    public Sexpression getSexpression()
    {
        return sexpression;
    }
    //
    public Row getRow()
    {
        return row;
    }
    //
    public Column getColumn()
    {
        return column;
    }
    //
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(symbol);
        builder.append(" ");
        builder.append(sexpression);
        builder.append(" in ");
        //
        if (row == null || column == null)
        {
            builder.append(" program");
        }
        else
        {
            builder.append(column);
            builder.append(row);
        }
        //
        return builder.toString();
    }
}