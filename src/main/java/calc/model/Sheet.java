package calc.model;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import lisp.CannotEvalException;
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.module.Module;
/*
 * Created by andreasm on 15.05.12 at 21:06
 */
public final class Sheet
{
    private final Row[] rows;
    private final Column[] columns;
    private final HashMap<Coordinates, Cell> cellsbycoordinates;
    private final CellListenerDelegate celllisteners;
    private final LinkedList<ProgramListener> programlisteners;
    private final ModuleRepository repository;
    private String program;
    private String modulename;
    private String modulecommentde;
    private Collection<String> moduleexports;
    //
    public Sheet()
    {
        super();
        //
        rows = new Row[64];
        columns = new Column[32];
        cellsbycoordinates = new HashMap<>(100);
        celllisteners = new CellListenerDelegate();
        programlisteners = new LinkedList<>();
        repository = new ModuleRepository();
        moduleexports = Collections.emptyList();
        //
        createColumns();
        createRows();
    }
    //
    public void addCellValueListener(CellValueListener listener)
    {
        celllisteners.addCellValueListener(listener);
    }
    //
    public void removeCellValueListener(CellValueListener listener)
    {
        celllisteners.removeCellValueListener(listener);
    }
    //
    public void addCellLockListener(CellLockListener listener)
    {
        celllisteners.addCellLockListener(listener);
    }
    //
    public synchronized void addProgramListener(ProgramListener listener)
    {
        programlisteners.add(listener);
    }
    //
    private void programChanged()
    {
        for (ProgramListener listener : programlisteners)
        {
            listener.programChanged();
        }
    }
    //
    public Row[] getRows()
    {
        return rows;
    }
    //
    public Column[] getColumns()
    {
        return columns;
    }
    //
    public synchronized Collection<Cell> getCells()
    {
        LinkedList<Cell> cells = new LinkedList<>();
        cells.addAll(cellsbycoordinates.values());
        //
        return cells;
    }
    //
    public synchronized void clear()
    {
        repository.clearModules();
        modulename = null;
        modulecommentde = null;
        moduleexports = Collections.emptyList();
        program = null;
        Collection<Cell> cells = cellsbycoordinates.values();
        //
        for (Cell cell : cells)
        {
            cell.setFormula("", 0L);
            cell.setLocked(false);
            cell.setUnchanged();
        }
        //
        programChanged();
    }
    //
    public synchronized boolean wasChanged()
    {
        for (Cell cell : cellsbycoordinates.values())
        {
            if (cell.wasChanged()) return true;
        }
        //
        return false;
    }
    //
    public synchronized void setUnchanged()
    {
        for (Cell cell : cellsbycoordinates.values())
        {
            cell.setUnchanged();
        }
    }
    //
    public void reevaluate()
    {
        Collection<Cell> cells = getCells();
        //
        for (Cell cell : cells)
        {
            cell.reevaluate();
        }
    }
    //
    public Cell findCell(String columnlabel, int rowindex)
    {
        try
        {
            int columnindex = Column.toIndex(columnlabel);
            //
            return findCell(columnindex, rowindex);
        }
        catch (IllegalArgumentException e)
        {
            // cell does not exist
        }
        //
        return null;
    }
    //
    public Cell findCell(int columnindex, int rowindex)
    {
        Column column = columns[columnindex];
        Row row = rows[rowindex];
        //
        return findCell(column, row);
    }
    //
    public Cell findCell(Sexpression sexpression) throws CannotEvalException
    {
        try
        {
            List list = (List) sexpression;
            Sexpression first = list.first();
            list = list.rest();
            Sexpression second = list.first();
            Column column = findColumn(first);
            Row row = findRow(second);
            //
            return findCell(column, row);
        }
        catch (NullPointerException e)
        {
            throw new CannotEvalException("coordinates for a cell must be a list with two non-nil elements");
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException("coordinates for a cell must be a list with a symbol and an integer");
        }
    }
    //
    public synchronized Cell findCell(Column column, Row row)
    {
        Coordinates coordinates = new Coordinates(row, column);
        Cell cell = cellsbycoordinates.get(coordinates);
        //
        if (cell == null)
        {
            cell = new Cell(celllisteners, column, row);
            cellsbycoordinates.put(coordinates, cell);
        }
        //
        return cell;
    }
    //
    public LinkedList<Cell> findCells(Sexpression sexpression) throws CannotEvalException
    {
        try
        {
            List list = (List) sexpression;
            Sexpression first = list.first();
            list = list.rest();
            Sexpression second = list.first();
            list = list.rest();
            Sexpression third = list.first();
            list = list.rest();
            Sexpression fourth = list.first();
            Column left = findColumn(first);
            Column right = findColumn(third);
            Row up = findRow(second);
            Row down = findRow(fourth);
            LinkedList<Cell> cells = new LinkedList<>();
            //
            for (int rowindex = up.getIndex(); rowindex <= down.getIndex(); rowindex++)
            {
                Row row = rows[rowindex];
                //
                for (int columnindex = left.getIndex(); columnindex <= right.getIndex(); columnindex++)
                {
                    Column column = columns[columnindex];
                    Cell cell = findCell(column, row);
                    cells.addFirst(cell);
                }
            }
            //
            return cells;
        }
        catch (NullPointerException e)
        {
            throw new CannotEvalException("coordinates for a cell range must be a list with four non-nil elements");
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException("coordinates for a cell must be a list with a symbol and an integer");
        }
    }
    //
    private void createColumns()
    {
        for (int index = 1; index < columns.length; index++)
        {
            String label = Column.toLabel(index);
            columns[index] = new Column(index, label);
        }
    }
    //
    private void createRows()
    {
        for (int index = 1; index < rows.length; index++)
        {
            rows[index] = new Row(index);
        }
    }
    //
    private synchronized Row findRow(Sexpression sexpression) throws CannotEvalException
    {
        Rational rational = (Rational) sexpression;
        int index = rational.intValue();
        //
        if (index < 1 || index >= rows.length) throw new CannotEvalException("invalid row");
        //
        Row row = rows[index];
        //
        if (row == null)
        {
            row = new Row(index);
            rows[index] = row;
        }
        //
        return row;
    }
    //
    private synchronized Column findColumn(Sexpression sexpression) throws CannotEvalException
    {
        Symbol symbol = (Symbol) sexpression;
        String label = symbol.getName();
        //
        try
        {
            int index = Column.toIndex(label);
            //
            if (index < 1 || index >= columns.length) throw new CannotEvalException("invalid column");
            //
            Column column = columns[index];
            //
            if (column == null)
            {
                column = new Column(index, label);
                columns[index] = column;
            }
            //
            return column;
        }
        catch (IllegalArgumentException e)
        {
            throw new CannotEvalException(e.getMessage());
        }
    }
    //
    public synchronized String getProgram()
    {
        return program;
    }
    //
    public synchronized void setProgram(String program)
    {
        this.program = program;
        programChanged();
        reevaluate();
    }
    //
    public synchronized Collection<Module> getModules()
    {
        return repository.getModules();
    }
    //
    public synchronized void setModules(Collection<Module> modules) throws IOException
    {
        repository.clearModules();
        repository.addModules(modules);
        repository.satisfyDependencies();
        programChanged();
        reevaluate();
    }
    //
    public synchronized String getModuleName()
    {
        return modulename;
    }
    //
    public synchronized void setModuleName(String modulename)
    {
        this.modulename = modulename;
    }
    //
    public synchronized String getModuleCommentDe()
    {
        return modulecommentde;
    }
    //
    public synchronized void setModuleCommentDe(String modulecommentde)
    {
        this.modulecommentde = modulecommentde;
    }
    //
    public synchronized Collection<String> getModuleExports()
    {
        return moduleexports;
    }
    //
    public synchronized void setModuleExports(Collection<String> moduleexports)
    {
        this.moduleexports = moduleexports;
    }
}