package calc;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
/*
 * Created by andreasm on 21.09.15.
 */
public final class Mediator
{
    private final HashMap<Class, Object> singletonbyclass;
    private final LinkedList<Object> objects;
    //
    public Mediator()
    {
        super();
        //
        singletonbyclass = new HashMap<>();
        objects = new LinkedList<>();
    }
    //
    @SuppressWarnings("unchecked")
    public void prepareSingleton(String classname)
    {
        try
        {
            Class clazz = Class.forName(classname);
            Object singleton;
            //
            try
            {
                singleton = clazz.newInstance();
            }
            catch (InstantiationException e)
            {
                Constructor constructor = clazz.getConstructor(Mediator.class);
                singleton = constructor.newInstance(this);
            }
            //
            singletonbyclass.put(clazz, singleton);
            objects.addLast(singleton);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException(e);
        }
    }
    //
    public <T> void putSingleton(Class<T> clazz, T singleton)
    {
        singletonbyclass.put(clazz, singleton);
        objects.addLast(singleton);
    }
    //
    @SuppressWarnings("unchecked")
    public <T> T getSingleton(Class<T> clazz)
    {
        Object singleton = singletonbyclass.get(clazz);
        //
        if (singleton == null) throw new IllegalStateException(String.format("singleton for %s not prepared or put", clazz));
        //
        return (T) singleton;
    }
    //
    @SuppressWarnings("unchecked")
    public <T> T createObject(Class<T> clazz, Object... restarguments)
    {
        try
        {
            Class[] parameterclasses = new Class[restarguments.length + 1];
            Object[] arguments = new Object[restarguments.length + 1];
            parameterclasses[0] = Mediator.class;
            arguments[0] = this;
            int i = 0;
            //
            while (i < restarguments.length)
            {
                Object argument = restarguments[i++];
                Class parameterclass = (argument == null) ? null : argument.getClass();
                parameterclasses[i] = parameterclass;
                arguments[i] = argument;
            }
            //
            Constructor<T>[] constructors = (Constructor<T>[]) clazz.getConstructors();
            Constructor<T> constructor = match(constructors, parameterclasses);
            T object = constructor.newInstance(arguments);
            objects.addLast(object);
            //
            return object;
        }
        catch (IllegalAccessException | InstantiationException | InvocationTargetException e)
        {
            throw new IllegalStateException(String.format("cannot create object for %s", clazz), e);
        }
    }
    //
    public <T> T createAndPostConstructObject(Class<T> clazz, Object... restarguments)
    {
        T object = createObject(clazz, restarguments);
        postConstruct();
        //
        return object;
    }
    //
    @SuppressWarnings("unchecked")
    public void postConstruct()
    {
        while (!objects.isEmpty())
        {
            Object object = objects.removeFirst();
            Class clazz = object.getClass();
            //
            try
            {
                Method method = clazz.getMethod("postConstruct");
                method.invoke(object);
            }
            catch (Exception e)
            {
                // do nothing
            }
        }
    }
    //
    private static <T> Constructor<T> match(Constructor<T>[] constructors, Class[] patternclasses)
    {
        for (Constructor<T> constructor : constructors)
        {
            if (match(constructor.getParameterTypes(), patternclasses)) return constructor;
        }
        //
        return null;
    }
    //
    @SuppressWarnings("unchecked")
    private static boolean match(Class[] parameterclasses, Class[] patternclasses)
    {
        if (parameterclasses.length != patternclasses.length) return false;
        //
        for (int i = 0; i < parameterclasses.length; i++)
        {
            Class patternclass = patternclasses[i];
            //
            if (patternclass == null) continue;
            //
            Class parameterclass = parameterclasses[i];
            //
            if (!parameterclass.isAssignableFrom(patternclass)) return false;
        }
        //
        return true;
    }
}