package calc;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Dimension;
import java.security.GeneralSecurityException;
import java.util.ResourceBundle;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.swing.*;
import calc.controller.Evaluator;
import calc.io.SelfSignedTrustManager;
import calc.view.AppFrame;
import lisp.combinator.EnvironmentFactory;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by  andreasm 19.05.12 20:23
 */
public final class CalcApp
{
    public static void main(String[] args)
    {
        if (System.getProperty("os.name").contains("Linux")) System.setProperty("sun.java2d.xrender", "True");
        //
        try
        {
            String lookandfeel = UIManager.getSystemLookAndFeelClassName();
            //
            if (lookandfeel.contains("apple"))
            {
                System.setProperty("apple.laf.useScreenMenuBar", "true");
                System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Calc");
            }
            //
            if (!lookandfeel.contains("GTK")) UIManager.setLookAndFeel(lookandfeel);
        }
        catch (IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException | ClassNotFoundException e)
        {
            // use standard Look and Feel
        }
        //
        String packages = System.getProperty("java.protocol.handler.pkgs", "");
        //
        if (!packages.contains("calc.io.streamhandler"))
        {
            if (!packages.isEmpty()) packages = packages + "|";
            //
            packages = packages + "calc.io.streamhandler";
            System.setProperty("java.protocol.handler.pkgs", packages);
        }
        //
        try
        {
            TrustManager trustmanager = new SelfSignedTrustManager();
            SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
            sslcontext.init(null, new TrustManager[] { trustmanager }, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
        }
        catch (GeneralSecurityException ignore)
        {
        }
        //
        EnvironmentFactory.createEnvironment(); // avoids a deadlock with awt-event-queue
        //
        invokeLater(() -> {
            Mediator mediator = new Mediator();
            /* create models */
            mediator.prepareSingleton("calc.model.ViewMode");
            mediator.prepareSingleton("calc.model.Sheet");
            mediator.prepareSingleton("calc.model.Calculation");
            mediator.prepareSingleton("calc.model.Log");
            mediator.prepareSingleton("calc.model.Selection");
            mediator.prepareSingleton("calc.model.Timeline");
            mediator.prepareSingleton("calc.model.FileReference");
            mediator.prepareSingleton("calc.model.ServiceModel");
            /* create evaluator */
            mediator.prepareSingleton("calc.controller.Evaluator");
            /* read resource bundle */
            mediator.putSingleton(ResourceBundle.class, ResourceBundle.getBundle("calc.view.ResourceBundle"));
            /* create actions */
            mediator.prepareSingleton("calc.controller.action.NewSheet");
            mediator.prepareSingleton("calc.controller.action.OpenSheet");
            mediator.prepareSingleton("calc.controller.action.SaveSheet");
            mediator.prepareSingleton("calc.controller.action.SaveSheetAs");
            mediator.prepareSingleton("calc.controller.action.SaveModule");
            mediator.prepareSingleton("calc.controller.action.ShowModules");
            mediator.prepareSingleton("calc.controller.action.ShowProgram");
            mediator.prepareSingleton("calc.controller.action.ChangeSheetLock");
            mediator.prepareSingleton("calc.controller.action.InterruptCalculation");
            mediator.prepareSingleton("calc.controller.action.CalculateSheet");
            mediator.prepareSingleton("calc.controller.action.MagnifyCell");
            mediator.prepareSingleton("calc.controller.action.ChangeCellLock");
            mediator.prepareSingleton("calc.controller.action.Undo");
            mediator.prepareSingleton("calc.controller.action.Redo");
            mediator.prepareSingleton("calc.controller.action.About");
            mediator.prepareSingleton("calc.controller.action.ApplyProgram");
            mediator.prepareSingleton("calc.controller.action.AddModule");
            mediator.prepareSingleton("calc.controller.action.ApplyModules");
            mediator.prepareSingleton("calc.controller.action.CancelProgramOrModules");
            mediator.prepareSingleton("calc.controller.action.SheetFileFilter");
            /* create views */
            mediator.prepareSingleton("calc.view.AppFrame");
            mediator.prepareSingleton("calc.view.ExceptionDialogFactory");
            mediator.prepareSingleton("calc.view.AppMenuBar");
            mediator.prepareSingleton("calc.view.SheetFormulaStatusPanel");
            mediator.prepareSingleton("calc.view.ProgramPanel");
            mediator.prepareSingleton("calc.view.ModulesPanel");
            mediator.prepareSingleton("calc.view.SheetPanel");
            mediator.prepareSingleton("calc.view.ColumnHeaderPanel");
            mediator.prepareSingleton("calc.view.RowHeaderPanel");
            mediator.prepareSingleton("calc.view.StatusLabel");
            mediator.prepareSingleton("calc.view.FormulaTextArea");
            mediator.prepareSingleton("calc.view.ProgramTextArea");
            /* create controllers */
            mediator.prepareSingleton("calc.controller.AppFrameController");
            mediator.prepareSingleton("calc.controller.SheetController");
            mediator.prepareSingleton("calc.controller.FormulaTextAreaController");
            mediator.prepareSingleton("calc.controller.ProgramTextAreaController");
            mediator.prepareSingleton("calc.controller.TimerController");
            /* register views as listeners of model events */
            /* register controllers as listeners of view events or model events */
            mediator.postConstruct();
            /* show frame */
            AppFrame frame = mediator.getSingleton(AppFrame.class);
            frame.pack();
            Dimension dimension = frame.getSize();
            frame.setMinimumSize(dimension);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            /* start evaluator */
            Evaluator evaluator = mediator.getSingleton(Evaluator.class);
            evaluator.start();
        });
    }
}