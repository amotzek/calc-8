package calc.view;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import javax.swing.*;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/**
 * Created by andreasm on 12.06.15
 */
public final class ExceptionDialogFactory
{
    private final Mediator mediator;
    //
    public ExceptionDialogFactory(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    public void showDialog(String titlekey, String messagekey, Exception e)
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        Icon icon = Style.getImageIcon("error.png");
        String title = bundle.getString(titlekey);
        Object[] message = new Object[] {
                bundle.getString(messagekey),
                " ",
                e.getClass().getSimpleName(),
                wrapLines(e.getLocalizedMessage())
        };
        //
        AppFrame appframe = mediator.getSingleton(AppFrame.class);
        JOptionPane.showMessageDialog(appframe, message, title, JOptionPane.ERROR_MESSAGE, icon);
    }
    //
    private static String wrapLines(String string)
    {
        StringBuilder builder = new StringBuilder();
        StringTokenizer tokenizer = new StringTokenizer(string, " ", true);
        int linelength = 0;
        //
        while (tokenizer.hasMoreTokens())
        {
            String token = tokenizer.nextToken();
            //
            if (" ".equals(token) && linelength == 0) continue;
            //
            linelength += token.length();
            builder.append(token);
            //
            if (linelength > 60)
            {
                builder.append("\n");
                linelength = 0;
            }
        }
        //
        return builder.toString();
    }
}