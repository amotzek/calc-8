package calc.view;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import calc.Mediator;
import calc.model.Cell;
import calc.model.CellValueListener;
import calc.model.Sheet;
import lisp.Sexpression;
import lisp.geometry.Picture;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by  andreasm 31.05.12 20:54
 */
public final class CellValueFrame extends JFrame implements CellValueListener
{
    private static final String PICTURE = "Picture";
    private static final String TEXT = "Text";
    //
    private final Mediator mediator;
    private final Cell cell;
    private final CardLayout layout;
    private final CellValueTextArea textarea;
    private final CellValuePicturePanel panel;
    //
    public CellValueFrame(Mediator mediator, Cell cell)
    {
        super();
        //
        this.mediator = mediator;
        this.cell = cell;
        //
        layout = new CardLayout();
        textarea = new CellValueTextArea();
        panel = new CellValuePicturePanel();
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        setTitle(MessageFormat.format(bundle.getString("CELL_RC"), cell.toString()));
        setLayout(layout);
        setBackground(Color.WHITE);
        add(textarea, TEXT);
        add(panel, PICTURE);
        setValueFrom(cell);
        //
        try
        {
            setIconImages(getFrameIconImages());
        }
        catch (Error e)
        {
            // use no icon
        }
        //
        sheet.addCellValueListener(this);
    }
    //
    @Override
    public void dispose()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        sheet.removeCellValueListener(this);
        //
        super.dispose();
    }
    //
    public void cellValueChanged(Cell changedcell)
    {
        if (changedcell != cell) return;
        //
        invokeLater(() -> CellValueFrame.this.setValueFrom(cell));
    }
    //
    private void setValueFrom(Cell cell)
    {
        Sexpression value = cell.getValue();
        String valuestring = cell.getValueString();
        Container contentpane = getContentPane();
        //
        if (value instanceof Picture)
        {
            Picture picture = (Picture) value;
            panel.setPicture(picture);
            layout.show(contentpane, PICTURE);
            textarea.setText("");
            //
            return;
        }
        //
        layout.show(contentpane, TEXT);
        panel.setPicture(null);
        textarea.setText(valuestring);
    }
}