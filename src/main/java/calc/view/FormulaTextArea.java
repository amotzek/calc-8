package calc.view;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.*;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.TransferHandler;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import calc.Mediator;
import calc.controller.action.CellOrCaretDown;
import calc.controller.action.CellOrCaretLeft;
import calc.controller.action.CellOrCaretRight;
import calc.controller.action.CellOrCaretUp;
import calc.controller.action.ChainableCellAction;
import calc.controller.transfer.FormulaTextAreaTransferHandler;
import calc.model.Cell;
import calc.model.CellLockListener;
import calc.model.CellSelectionListener;
import calc.model.Selection;
import calc.model.Sheet;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
import static javax.swing.SwingUtilities.isEventDispatchThread;
/*
 * Created by andreasm on 23.05.12 at 12:09
 */
public final class FormulaTextArea extends JTextArea implements CellSelectionListener, CellLockListener
{
    private final Mediator mediator;
    private volatile Cell owner;
    //
    public FormulaTextArea(Mediator mediator)
    {
        super(10, 40);
        //
        this.mediator = mediator;
        //
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        setDropTarget(null);
        setFont(getMonospacedFont());
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Selection selection = mediator.getSingleton(Selection.class);
        sheet.addCellLockListener(this);
        selection.addCellSelectionListener(this);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int keymask = toolkit.getMenuShortcutKeyMask();
        KeyStroke left = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0);
        KeyStroke right = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0);
        KeyStroke up = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0);
        KeyStroke down = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0);
        KeyStroke ctrlz = KeyStroke.getKeyStroke(KeyEvent.VK_Z, keymask);
        InputMap inputmap = getInputMap();
        ActionMap actionmap = getActionMap();
        CellOrCaretLeft cellorcaretleft = new CellOrCaretLeft(sheet, selection);
        CellOrCaretRight cellorcaretright = new CellOrCaretRight(sheet, selection);
        CellOrCaretUp cellorcaretup = new CellOrCaretUp(sheet, selection);
        CellOrCaretDown cellorcaretdown = new CellOrCaretDown(sheet, selection);
        chainAction(inputmap, actionmap, left, cellorcaretleft);
        chainAction(inputmap, actionmap, right, cellorcaretright);
        chainAction(inputmap, actionmap, up, cellorcaretup);
        chainAction(inputmap, actionmap, down, cellorcaretdown);
        removeAction(inputmap, ctrlz);
        TransferHandler transferhandler = getTransferHandler();
        setTransferHandler(mediator.createObject(FormulaTextAreaTransferHandler.class, transferhandler));
    }
    //
    private static void chainAction(InputMap inputmap, ActionMap actionmap, KeyStroke keystroke, ChainableCellAction chainableaction)
    {
        Object actionid = inputmap.get(keystroke);
        Action action = actionmap.get(actionid);
        chainableaction.setNextAction(action);
        actionmap.put(actionid, chainableaction);
    }
    //
    private static void removeAction(InputMap inputmap, KeyStroke keystroke)
    {
        inputmap.remove(keystroke);
    }
    //
    public Cell getCell()
    {
        return owner;
    }
    //
    public void cellSelected(final Cell cell, final boolean toporleft)
    {
        invokeLater(() -> FormulaTextArea.this.changeOwningCell(cell, toporleft));
    }
    //
    public void cellUnselected(final Cell cell)
    {
        // Sonderbehandlung sorgt dafür,
        // dass ein Block korrekt eingefügt
        // werden kann
        if (isEventDispatchThread())
        {
            copyTextTo(cell);
            //
            return;
        }
        //
        invokeLater(() -> FormulaTextArea.this.copyTextTo(cell));
    }
    //
    public void cellRangeChanged(Cell cell1, Cell cell2)
    {
    }
    //
    public void cellLockChanged(final Cell cell)
    {
        invokeLater(() -> changeLock(cell));
    }
    //
    private void changeOwningCell(Cell cell, boolean toporleft)
    {
        if (owner != null) throw new IllegalStateException();
        //
        String formula = cell.getFormula();
        boolean locked = cell.isLocked();
        setText(formula);
        owner = cell;
        setEnabled(true);
        //
        if (locked)
        {
            if (isEditable())
            {
                setBackground(LOCK_COLOR);
                setEditable(false);
                Caret caret = getCaret();
                caret.setVisible(true);
            }
            //
            if (toporleft) setCaretPosition(0);
            //
            requestFocus();
            //
            return;
        }
        //
        if (!isEditable())
        {
            setEditable(true);
            setBackground(Color.WHITE);
        }
        //
        if (hasSmallText())
        {
            if (formula != null && toporleft)
            {
                setCaretPosition(formula.length());
                moveCaretPosition(0);
            }
            else
            {
                selectAll();
            }
        }
        else if (toporleft)
        {
            setCaretPosition(0);
        }
        //
        requestFocus();
    }
    //
    private boolean hasSmallText()
    {
        try
        {
            return getLineCount() == 1 && getLineEndOffset(0) < 15;
        }
        catch (BadLocationException e)
        {
            //
        }
        //
        return true;
    }
    //
    private void copyTextTo(Cell cell)
    {
        if (cell != owner) throw new IllegalStateException();
        //
        String formula = getText();
        long lastupdate = cell.getLastUpdate();
        cell.setFormula(formula, lastupdate);
        setText("");
        setEnabled(false);
        owner = null;
    }
    //
    private void changeLock(Cell cell)
    {
        if (cell != owner) return;
        //
        boolean editable = !cell.isLocked();
        //
        if (isEditable() == editable) return;
        //
        setBackground(editable ? Color.WHITE : LOCK_COLOR);
        setEditable(editable);
        Caret caret = getCaret();
        caret.setVisible(true);
    }
}