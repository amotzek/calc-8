package calc.view;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import javax.swing.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
import static calc.view.Style.ERROR_COLOR;
import static calc.view.Style.getPlainFont;
/**
 * Created by Andreas on 11.06.2015
 */
public final class ModuleSearchDialog extends JDialog
{
    private final Mediator mediator;
    private final JTextField urifield;
    private final JOptionPane optionpane;
    //
    public ModuleSearchDialog(Mediator mediator)
    {
        super(mediator.getSingleton(AppFrame.class), true);
        //
        this.mediator = mediator;
        //
        urifield = createTextField();
        optionpane = createOptionPane(urifield);
        //
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        optionpane.addPropertyChangeListener(JOptionPane.VALUE_PROPERTY, event -> {
            Object value = event.getNewValue();
            //
            if (isOK(value))
            {
                try
                {
                    getURI();
                }
                catch (URISyntaxException e)
                {
                    markError(urifield);
                    clearValue();
                    //
                    return;
                }
            }
            //
            if (isInitialized(value)) dispose();
        });
        //
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        setTitle(bundle.getString("ADD_MODULE"));
        setContentPane(optionpane);
        setResizable(false);
        pack();
        setLocationRelativeTo(getOwner());
    }
    //
    public Object getValue()
    {
        return optionpane.getValue();
    }
    //
    public boolean isOK(Object value)
    {
        return value instanceof Integer && JOptionPane.OK_OPTION == (Integer) value;
    }
    //
    private boolean isInitialized(Object value)
    {
        return value != JOptionPane.UNINITIALIZED_VALUE;
    }
    //
    public URI getURI() throws URISyntaxException
    {
        String text = urifield.getText();
        //
        return new URI(text);
    }
    //
    private static void markError(JComponent component)
    {
        component.setBackground(ERROR_COLOR);
    }
    //
    private void clearValue()
    {
        optionpane.setValue(JOptionPane.UNINITIALIZED_VALUE);
    }
    //
    private static JTextField createTextField()
    {
        JTextField field = new JTextField(40);
        field.setFont(getPlainFont());
        //
        return field;
    }
    //
    private JOptionPane createOptionPane(JTextField urlfield)
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        Object[] message = new Object[] {
                bundle.getString("SPECIFY_MODULE"),
                urlfield
        };
        //
        return new JOptionPane(message, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
    }
}