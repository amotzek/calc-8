package calc.view;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
/*
 * Created by  andreasm 21.05.12 18:49
 */
final class TextPainter
{
    static final int LEFT = 0;
    static final int CENTER = 1;
    static final int RIGHT = 2;
    //
    private final int alignment;
    //
    TextPainter(int alignment)
    {
        super();
        //
        this.alignment = alignment;
    }
    //
    void drawStringIn(Graphics graphics, Rectangle rectangle, String string)
    {
        Shape clip = graphics.getClip();
        Rectangle clipbounds = clip.getBounds();
        FontMetrics metrics = graphics.getFontMetrics();
        int stringwidth = metrics.stringWidth(string);
        int stringheigth = metrics.getHeight();
        //
        if (stringwidth > rectangle.width)
        {
            int xmid = rectangle.x + (rectangle.width >> 1);
            int ymid = rectangle.y + (rectangle.height >> 1);
            Polygon polygon = new Polygon();
            polygon.addPoint(xmid, rectangle.y + 4);
            polygon.addPoint(xmid + 4, ymid);
            polygon.addPoint(xmid, rectangle.y + rectangle.height - 4);
            polygon.addPoint(xmid - 4, ymid);
            Graphics2D graphics2 = (Graphics2D) graphics;
            graphics2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2.drawPolygon(polygon);
            //
            return;
        }
        //
        int x = rectangle.x;
        int y = rectangle.y;
        //
        switch (alignment)
        {
            case LEFT:
                //
                break;
            case RIGHT:
                x += rectangle.width;
                x -= stringwidth;
                //
                break;
            case CENTER:
                x += (rectangle.width - stringwidth) >> 1;
                //
                break;
        }
        //
        y += stringheigth;
        Rectangle2D intersection = rectangle.createIntersection(clipbounds);
        Graphics2D graphics2 = (Graphics2D) graphics;
        graphics2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics2.setClip(intersection);
        graphics2.drawString(string, x, y);
        graphics2.setClip(clip);
    }
}