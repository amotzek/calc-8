package calc.view;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import javax.swing.JMenu;
import calc.Mediator;
import calc.model.ViewMode;
import calc.model.ViewModeListener;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by andreasm
 * Date: 22.11.13
 * Time: 08:11
 */
public final class SheetModeMenu extends JMenu implements ViewModeListener
{
    private final Mediator mediator;
    //
    public SheetModeMenu(Mediator mediator, String title)
    {
        super(title);
        //
        this.mediator = mediator;
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        ViewMode viewmode = mediator.getSingleton(ViewMode.class);
        viewmode.addViewModeListener(this);
    }
    //
    public void viewModeChanged()
    {
        ViewMode viewmode = mediator.getSingleton(ViewMode.class);
        String modename = viewmode.getModeName();
        final boolean enabled = ViewMode.SHEET.equals(modename);
        //
        invokeLater(() -> SheetModeMenu.this.setEnabled(enabled));
    }
}