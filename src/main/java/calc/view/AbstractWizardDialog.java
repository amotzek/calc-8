package calc.view;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.model.WizardModel;
import calc.model.WizardModelListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ResourceBundle;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/**
 * Created by andreasm on 16.09.18
 */
public abstract class AbstractWizardDialog extends JDialog implements WizardModelListener
{
    protected final Mediator mediator;
    private final WizardModel model;
    private final CardLayout layout;
    private final JPanel cardpanel;
    private final Box buttonbox;
    private final JButton backbutton;
    private final JButton nextbutton;
    //
    AbstractWizardDialog(Mediator mediator, WizardModel model)
    {
        super(mediator.getSingleton(AppFrame.class), true);
        //
        this.mediator = mediator;
        this.model = model;
        //
        layout = new CardLayout();
        cardpanel = new JPanel();
        buttonbox = Box.createHorizontalBox();
        backbutton = new JButton();
        nextbutton = new JButton();
    }
    //
    public void postConstruct()
    {
        model.addWizardModelListener(this);
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        backbutton.setText(bundle.getString("BACK"));
        nextbutton.setText(bundle.getString("NEXT"));
        buttonbox.setBorder(new EmptyBorder(new Insets(Style.GAP, Style.GAP, Style.GAP, Style.GAP)));
        buttonbox.add(Box.createGlue());
        buttonbox.add(backbutton);
        buttonbox.add(Box.createHorizontalStrut(Style.GAP));
        buttonbox.add(nextbutton);
        cardpanel.setLayout(layout);
        Container container = getContentPane();
        container.add(cardpanel, BorderLayout.CENTER);
        container.add(buttonbox, BorderLayout.SOUTH);
    }
    //
    final void addStep(JComponent component, String step)
    {
        cardpanel.add(component, step);
    }
    //
    @Override
    public void stepChanged()
    {
        String step = model.getStep();
        invokeLater(() -> AbstractWizardDialog.this.showStep(step));
    }
    //
    private void showStep(String step)
    {
        layout.show(cardpanel, step);
    }
    //
    public final JButton getBackButton()
    {
        return backbutton;
    }
    //
    public final JButton getNextButton()
    {
        return nextbutton;
    }
    //
    final JLabel createLabel(String name)
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        JLabel label = new JLabel();
        label.setText(bundle.getString(name));
        label.setFont(getSmallFont());
        label.setAlignmentX(Component.LEFT_ALIGNMENT);
        growOnlyHorizontally(label);
        //
        return label;
    }
    //
    static JTextPane createEmptyTextPane()
    {
        JTextPane pane = new JTextPane();
        pane.setOpaque(false);
        pane.setEditable(false);
        pane.setFont(getPlainFont());
        pane.setAlignmentX(Component.LEFT_ALIGNMENT);
        //
        return pane;
    }
    //
    final JTextPane createTextPane(String name)
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        JTextPane pane = createEmptyTextPane();
        pane.setText(bundle.getString(name));
        growNever(pane);
        //
        return pane;
    }
    //
    static JTextField createTextField(String text)
    {
        JTextField field = new JTextField(text, 40);
        field.setFont(getPlainFont());
        field.setAlignmentX(Component.LEFT_ALIGNMENT);
        growOnlyHorizontally(field);
        //
        return field;
    }
    //
    static JTextField createPasswordField(char[] text)
    {
        JPasswordField field = new JPasswordField(new String(text), 40);
        field.setFont(getPlainFont());
        field.setAlignmentX(Component.LEFT_ALIGNMENT);
        growOnlyHorizontally(field);
        //
        return field;
    }
    //
    private static void growNever(JComponent component)
    {
        Dimension maximum = component.getPreferredSize();
        component.setMaximumSize(maximum);
    }
    //
    private static void growOnlyHorizontally(JComponent component)
    {
        Dimension preferred = component.getPreferredSize();
        Dimension maximum = new Dimension(Integer.MAX_VALUE, preferred.height);
        component.setMaximumSize(maximum);
    }
    //
    private static JTextArea createTextArea(String text, Font font)
    {
        JTextArea area = new JTextArea(text, 3, 40);
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setFont(font);
        //
        return area;
    }
    //
    static JTextArea createPlainTextArea(String text)
    {
        return createTextArea(text, getPlainFont());
    }
    //
    static JTextArea createMonospacedTextArea(String text)
    {
        return createTextArea(text, getMonospacedFont());
    }
    //
    final JComboBox<String> createComboBox(String... names)
    {
        JComboBox<String> combobox = new JComboBox<>();
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        //
        for (String name : names)
        {
            combobox.addItem(bundle.getString(name));
        }
        //
        combobox.setBackground(Color.WHITE);
        combobox.setAlignmentX(Component.LEFT_ALIGNMENT);
        growOnlyHorizontally(combobox);
        //
        return combobox;
    }
    //
    static void markError(JComponent component, boolean error)
    {
        component.setBackground(error ? ERROR_COLOR : Color.WHITE);
    }
}
