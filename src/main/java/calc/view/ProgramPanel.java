package calc.view;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import calc.Mediator;
import calc.controller.action.ApplyProgram;
import calc.controller.action.CancelProgramOrModules;
import calc.model.Sheet;
import static calc.view.Style.*;
/*
 * User: andreasm
 * Date: 18.04.13
 * Time: 08:15
 */
public final class ProgramPanel extends JPanel
{
    private final Mediator mediator;
    private final JButton applybutton;
    private final JButton cancelbutton;
    //
    public ProgramPanel(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
        //
        applybutton = createButton();
        cancelbutton = createButton();
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        setLayout(new GridBagLayout());
        ProgramTextArea textarea = mediator.getSingleton(ProgramTextArea.class);
        JScrollPane scrollpane = createScrollPane(textarea);
        add(scrollpane, new GridBagConstraints(0, 0, 3, 1, 1.0d, 1.0d, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, SMALL_GAP, 0), 0, 0));
        add(applybutton, new GridBagConstraints(0, 1, 1, 1, 1.0d, 0.0d, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, SMALL_GAP, GAP), 0, 0));
        add(cancelbutton, new GridBagConstraints(1, 1, 1, 1, 0.0d, 0.0d, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, SMALL_GAP, SMALL_GAP), 0, 0));
        applybutton.setAction(mediator.getSingleton(ApplyProgram.class));
        cancelbutton.setAction(mediator.getSingleton(CancelProgramOrModules.class));
        adaptSize(applybutton, cancelbutton);
    }
    //
    public void copyFromSheet()
    {
        ProgramTextArea textarea = mediator.getSingleton(ProgramTextArea.class);
        Sheet sheet = mediator.getSingleton(Sheet.class);
        String library = sheet.getProgram();
        textarea.setText(library);
    }
    //
    public void copyToSheet()
    {
        ProgramTextArea textarea = mediator.getSingleton(ProgramTextArea.class);
        Sheet sheet = mediator.getSingleton(Sheet.class);
        String library = textarea.getText();
        sheet.setProgram(library);
    }
    //
    private static JButton createButton()
    {
        JButton button = new JButton();
        button.setFont(getSmallFont());
        //
        return button;
    }
}