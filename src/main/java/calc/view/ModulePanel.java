package calc.view;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.controller.ModulePanelController;
import calc.controller.action.DeleteModule;
import lisp.module.Module;
import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.ResourceBundle;
import static calc.view.Style.*;
/**
 * Created by andreasm on 07.06.15
 */
public final class ModulePanel extends JPanel
{
    private final Mediator mediator;
    private final Module module;
    private final JButton deletebutton;
    //
    public ModulePanel(Mediator mediator, Module module)
    {
        super();
        //
        this.mediator = mediator;
        this.module = module;
        //
        deletebutton = createButton();
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        JLabel namelabel = createLabel("NAME");
        JLabel versionlabel = createLabel("VERSION");
        JLabel descriptionlabel = createLabel("DESCRIPTION");
        JLabel exportslabel = createLabel("MODULE_EXPORTS");
        JLabel bodylabel = createLabel("MODULE_BODY");
        JTextField namefield = createTextField(module.getName());
        JTextField versionfield = createTextField(Integer.toString(module.getVersion()));
        JTextArea commentdearea = createTextArea(module.getCommentDe(), getPlainFont());
        JTextArea exportsarea = createTextArea(toString(module.getExports()), getMonospacedFont());
        JTextArea bodyarea = createTextArea(module.getBody(), getMonospacedFont());
        setLayout(new GridBagLayout());
        setOpaque(false);
        add(namelabel, new GridBagConstraints(0, 0, 1, 1, 0d, 0d, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(GAP, GAP, SMALL_GAP, 0), 0, 0));
        add(namefield, new GridBagConstraints(0, 1, 1, 1, 0.80d, 0d, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, GAP, GAP, GAP), 0, 0));
        add(versionlabel, new GridBagConstraints(1, 0, 1, 1, 0d, 0d, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(GAP, GAP, SMALL_GAP, 0), 0, 0));
        add(versionfield, new GridBagConstraints(1, 1, 1, 1, 0.20d, 0d, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, GAP, GAP, GAP), 0, 0));
        add(deletebutton, new GridBagConstraints(2, 0, 1, 2, 0d, 0d, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0, GAP, GAP, GAP), 0, 0));
        add(descriptionlabel, new GridBagConstraints(0, 2, 3, 1, 0d, 0d, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, GAP, SMALL_GAP, 0), 0, 0));
        add(commentdearea, new GridBagConstraints(0, 3, 3, 1, 1d, 0d, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, GAP, GAP, GAP), 0, 0));
        add(exportslabel, new GridBagConstraints(0, 4, 3, 1, 0d, 0d, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, GAP, SMALL_GAP, 0), 0, 0));
        add(exportsarea, new GridBagConstraints(0, 5, 3, 1, 1d, 0d, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, GAP, GAP, GAP), 0, 0));
        add(bodylabel, new GridBagConstraints(0, 6, 3, 1, 0d, 0d, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, GAP, SMALL_GAP, 0), 0, 0));
        add(bodyarea, new GridBagConstraints(0, 7, 3, 1, 1d, 0d, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, GAP, GAP, GAP), 0, 0));
        ModulePanelController controller = new ModulePanelController(this);
        DeleteModule deletemodule = new DeleteModule(this);
        addMouseListener(controller);
        deletebutton.setAction(deletemodule);
        deletebutton.setEnabled(false);
    }
    //
    public void setSelected(boolean selected)
    {
        if (selected)
        {
            if (isOpaque()) return;
            //
            deletebutton.setEnabled(true);
            setBackground(MODULE_SELECTION_COLOR);
            setOpaque(true);
            //
            return;
        }
        //
        setOpaque(false);
        setBackground(null);
        deletebutton.setEnabled(false);
    }
    //
    public Module getModule()
    {
        return module;
    }
    //
    private JLabel createLabel(String name)
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        JLabel label = new JLabel();
        label.setFont(getSmallFont());
        label.setText(bundle.getString(name));
        //
        return label;
    }
    //
    private static JButton createButton()
    {
        JButton button = new JButton();
        button.setBorderPainted(false);
        button.setBorder(null);
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setPreferredSize(new Dimension(20, 20));
        button.setContentAreaFilled(false);
        button.setOpaque(false);
        button.setBackground(null);
        //
        return button;
    }
    //
    private static JTextField createTextField(String text)
    {
        JTextField field = new JTextField();
        field.setEditable(false);
        field.setBorder(null);
        field.setOpaque(false);
        field.setFont(getPlainFont());
        field.setText(text);
        //
        return field;
    }
    //
    private static JTextArea createTextArea(String text, Font font)
    {
        JTextArea area = new JTextArea();
        area.setEditable(false);
        area.setBorder(null);
        area.setOpaque(false);
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setFont(font);
        area.setText(text);
        //
        return area;
    }
    //
    private static String toString(Collection<String> exports)
    {
        StringBuilder builder = new StringBuilder();
        //
        for (String export : exports)
        {
            if (builder.length() > 0) builder.append(' ');
            //
            builder.append(export);
        }
        //
        return builder.toString();
    }
}