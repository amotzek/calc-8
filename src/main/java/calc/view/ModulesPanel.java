package calc.view;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.controller.action.AddModule;
import calc.controller.action.CancelProgramOrModules;
import calc.controller.action.ApplyModules;
import calc.model.Log;
import calc.model.Sheet;
import lisp.CannotEvalException;
import lisp.module.Module;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * User: andreasm
 * Date: 18.04.13
 * Time: 08:15
 */
public final class ModulesPanel extends JPanel
{
    private static final Rectangle TOP = new Rectangle(0, 0, 1, 1);
    //
    private final Mediator mediator;
    private final JPanel panel;
    private final JButton addmodulebutton;
    private final JButton applybutton;
    private final JButton cancelbutton;
    //
    public ModulesPanel(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
        //
        panel = createPanel();
        addmodulebutton = createButton();
        applybutton = createButton();
        cancelbutton = createButton();
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        setLayout(new GridBagLayout());
        JScrollPane scrollpane = createScrollPane(panel);
        JScrollBar scrollbar = scrollpane.getVerticalScrollBar();
        scrollbar.setUnitIncrement(20);
        InputMap inputmap = scrollbar.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputmap.put(KeyStroke.getKeyStroke("DOWN"), "positiveUnitIncrement");
        inputmap.put(KeyStroke.getKeyStroke("UP"), "negativeUnitIncrement");
        add(scrollpane, new GridBagConstraints(0, 0, 3, 1, 1d, 1d, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, SMALL_GAP, 0), 0, 0));
        add(addmodulebutton, new GridBagConstraints(0, 1, 1, 1, 0d, 0d, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, SMALL_GAP, 0, GAP), 0, 0));
        add(applybutton, new GridBagConstraints(1, 1, 1, 1, 1d, 0d, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, SMALL_GAP, GAP), 0, 0));
        add(cancelbutton, new GridBagConstraints(2, 1, 1, 1, 0d, 0d, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, SMALL_GAP, SMALL_GAP), 0, 0));
        addmodulebutton.setAction(mediator.getSingleton(AddModule.class));
        applybutton.setAction(mediator.getSingleton(ApplyModules.class));
        cancelbutton.setAction(mediator.getSingleton(CancelProgramOrModules.class));
        adaptSize(addmodulebutton);
        adaptSize(applybutton, cancelbutton);
    }
    //
    public void copyFromSheet()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Collection<Module> modules = sheet.getModules();
        panel.removeAll();
        //
        for (Module module : modules)
        {
            ModulePanel modulepanel = mediator.createObject(ModulePanel.class, module);
            panel.add(modulepanel);
        }
        //
        mediator.postConstruct();
        panel.validate();
        panel.requestFocus();
        //
        invokeLater(() -> panel.scrollRectToVisible(TOP));
    }
    //
    public JComponent addOrUpdateModule(Module module)
    {
        int index = getComponentIndex(module);
        //
        if (index >= 0)
        {
            ModulePanel modulepanel = mediator.createObject(ModulePanel.class, module);
            mediator.postConstruct();
            panel.remove(index);
            panel.add(modulepanel, index);
            //
            return modulepanel;
        }
        //
        ModulePanel modulepanel = mediator.createObject(ModulePanel.class, module);
        mediator.postConstruct();
        panel.add(modulepanel);
        //
        return modulepanel;
    }
    //
    private int getComponentIndex(Module module1)
    {
        String name1 = module1.getName();
        //
        for (int i = 0; i < panel.getComponentCount(); i++)
        {
            Component component = panel.getComponent(i);
            //
            if (component instanceof ModulePanel)
            {
                ModulePanel modulepanel = (ModulePanel) component;
                Module module2 = modulepanel.getModule();
                String name2 = module2.getName();
                //
                if (name1.equals(name2)) return i;
            }
        }
        //
        return -1;
    }
    //
    public void validateAndScroll(JComponent modulepanel)
    {
        panel.validate();
        panel.scrollRectToVisible(modulepanel.getBounds());
    }
    //
    public void copyToSheet()
    {
        try
        {
            Sheet sheet = mediator.getSingleton(Sheet.class);
            Collection<Module> modules = getModules();
            sheet.setModules(modules);
        }
        catch (IOException e)
        {
            Log log = mediator.getSingleton(Log.class);
            log.setProgramException(new CannotEvalException("cannot satisfy module dependencies: " + e.getMessage()));
        }
    }
    //
    public Collection<Module> getModules()
    {
        LinkedList<Module> modules = new LinkedList<>();
        //
        for (int i = 0; i < panel.getComponentCount(); i++)
        {
            Component component = panel.getComponent(i);
            //
            if (component instanceof ModulePanel)
            {
                ModulePanel modulepanel = (ModulePanel) component;
                Module module = modulepanel.getModule();
                modules.addLast(module);
            }
        }
        //
        return modules;
    }
    //
    private static JPanel createPanel()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBackground(Color.WHITE);
        //
        return panel;
    }
    //
    private static JButton createButton()
    {
        JButton button = new JButton();
        button.setFont(getSmallFont());
        //
        return button;
    }
}