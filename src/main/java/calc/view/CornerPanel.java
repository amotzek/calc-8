package calc.view;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.SystemColor;
import javax.swing.JPanel;
/*
 * Created by andreasm on 27.05.12 at 13:14
 */
final class CornerPanel extends JPanel
{
    private final boolean top;
    private final boolean bottom;
    private final boolean left;
    private final boolean right;
    //
    CornerPanel(boolean left, boolean right, boolean top, boolean bottom)
    {
        super();
        //
        this.right = right;
        this.left = left;
        this.bottom = bottom;
        this.top = top;
    }
    //
    @Override
    protected void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);
        Dimension dimension = getSize();
        int w = dimension.width - 1;
        int h = dimension.height - 1;
        graphics.setColor(SystemColor.controlShadow);
        //
        if (left) graphics.drawLine(0, 0, 0, h);
        if (right) graphics.drawLine(w, 0, w, h);
        if (top) graphics.drawLine(0, 0, w, 0);
        if (bottom) graphics.drawLine(0, h, w, h);
        //
        graphics.setColor(getForeground());
    }
}