package calc.view;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.CardLayout;
import java.awt.Container;
import java.io.File;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import calc.Mediator;
import calc.model.*;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by  andreasm 19.05.12 20:18
 */
public final class AppFrame extends JFrame implements FileListener, ViewModeListener
{
    private final Mediator mediator;
    private final CardLayout layout;
    //
    public AppFrame(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
        //
        layout = new CardLayout();
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        FileReference filereference = mediator.getSingleton(FileReference.class);
        ViewMode viewmode = mediator.getSingleton(ViewMode.class);
        AppMenuBar appmenubar = mediator.getSingleton(AppMenuBar.class);
        SheetFormulaStatusPanel sheetformulastatuspanel = mediator.getSingleton(SheetFormulaStatusPanel.class);
        ProgramPanel programpanel = mediator.getSingleton(ProgramPanel.class);
        ModulesPanel modulespanel = mediator.getSingleton(ModulesPanel.class);
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        setTitle(bundle.getString("SHEET"));
        //
        try
        {
            setIconImages(getFrameIconImages());
        }
        catch (Error e)
        {
            // no icon
        }
        //
        setLayout(layout);
        filereference.addFileListener(this);
        viewmode.addViewModeListener(this);
        setJMenuBar(appmenubar);
        add(sheetformulastatuspanel, ViewMode.SHEET);
        add(programpanel, ViewMode.PROGRAM);
        add(modulespanel, ViewMode.MODULES);
        showCard(ViewMode.SHEET);
    }
    //
    public void fileChanged()
    {
        FileReference filereference = mediator.getSingleton(FileReference.class);
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        File file = filereference.getFile();
        final String title = (file == null) ? bundle.getString("SHEET") : MessageFormat.format(bundle.getString("SHEET_FILE"), file.getName());
        //
        invokeLater(() -> AppFrame.this.setTitle(title));
    }
    //
    public void viewModeChanged()
    {
        ViewMode viewmode = mediator.getSingleton(ViewMode.class);
        final String modename = viewmode.getModeName();
        //
        invokeLater(() -> AppFrame.this.showCard(modename));
    }
    //
    private void showCard(String modename)
    {
        Container contentpane = getContentPane();
        layout.show(contentpane, modename);
    }
}