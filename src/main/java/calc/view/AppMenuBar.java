package calc.view;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.ResourceBundle;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import calc.Mediator;
import calc.controller.action.*;
/*
 * Created by andreasm on 29.09.2014.
 */
public final class AppMenuBar extends JMenuBar
{
    private final Mediator mediator;
    //
    public AppMenuBar(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        add(createFileMenu());
        add(createSheetMenu());
        add(createCellMenu());
        add(createExtrasMenu());
    }
    //
    private SheetModeMenu createFileMenu()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        String name = bundle.getString("FILE");
        SheetModeMenu menu = mediator.createObject(SheetModeMenu.class, name);
        menu.add(mediator.getSingleton(NewSheet.class));
        menu.add(mediator.getSingleton(OpenSheet.class));
        menu.add(mediator.getSingleton(SaveSheet.class));
        menu.add(mediator.getSingleton(SaveSheetAs.class));
        menu.addSeparator();
        menu.add(mediator.getSingleton(SaveModule.class));
        //
        return menu;
    }
    //
    private SheetModeMenu createSheetMenu()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        String name = bundle.getString("SHEET");
        SheetModeMenu menu = mediator.createObject(SheetModeMenu.class, name);
        menu.add(mediator.getSingleton(ShowModules.class));
        menu.add(mediator.getSingleton(ShowProgram.class));
        menu.add(mediator.getSingleton(ChangeSheetLock.class));
        menu.addSeparator();
        menu.add(mediator.getSingleton(InterruptCalculation.class));
        menu.add(mediator.getSingleton(CalculateSheet.class));
        //
        return menu;
    }
    //
    private SheetModeMenu createCellMenu()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        String name = bundle.getString("CELL");
        SheetModeMenu menu = mediator.createObject(SheetModeMenu.class, name);
        menu.add(mediator.getSingleton(MagnifyCell.class));
        menu.add(mediator.getSingleton(ChangeCellLock.class));
        menu.addSeparator();
        menu.add(mediator.getSingleton(Undo.class));
        menu.add(mediator.getSingleton(Redo.class));
        //
        return menu;
    }
    //
    private JMenu createExtrasMenu()
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        String name = bundle.getString("EXTRAS");
        JMenu menu = new JMenu(name);
        menu.add(mediator.getSingleton(About.class));
        //
        return menu;
    }
}