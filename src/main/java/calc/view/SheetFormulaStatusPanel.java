package calc.view;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import calc.Mediator;
import static calc.view.Style.*;
/*
 * User: andreasm
 * Date: 17.04.13
 * Time: 18:20
 */
public final class SheetFormulaStatusPanel extends JPanel
{
    private static final Insets NO_INSETS = new Insets(0, 0, 0, 0);
    //
    private final Mediator mediator;
    //
    public SheetFormulaStatusPanel(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);
        setBackground(Color.WHITE);
        SheetPanel sheetpanel = mediator.getSingleton(SheetPanel.class);
        ColumnHeaderPanel columnheaderpanel = mediator.getSingleton(ColumnHeaderPanel.class);
        RowHeaderPanel rowheaderpanel = mediator.getSingleton(RowHeaderPanel.class);
        FormulaTextArea formulatextarea = mediator.getSingleton(FormulaTextArea.class);
        StatusLabel statuslabel = mediator.getSingleton(StatusLabel.class);
        JScrollPane sheetscrollpane = createScrollPane(sheetpanel);
        CornerPanel upperleftcorner = new CornerPanel(false, true, false, true);
        CornerPanel upperrightcorner = new CornerPanel(true, false, false, true);
        CornerPanel lowerleftcorner = new CornerPanel(false, true, true, false);
        Dimension preferredsize = calculatePreferredScrollPaneSize();
        sheetscrollpane.setPreferredSize(preferredsize);
        sheetscrollpane.setColumnHeaderView(columnheaderpanel);
        sheetscrollpane.setRowHeaderView(rowheaderpanel);
        sheetscrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        sheetscrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        sheetscrollpane.setBackground(SystemColor.control);
        sheetscrollpane.setCorner(JScrollPane.UPPER_LEFT_CORNER, upperleftcorner);
        sheetscrollpane.setCorner(JScrollPane.UPPER_RIGHT_CORNER, upperrightcorner);
        sheetscrollpane.setCorner(JScrollPane.LOWER_LEFT_CORNER, lowerleftcorner);
        sheetscrollpane.getHorizontalScrollBar().setUnitIncrement(COLUMN_WIDTH);
        sheetscrollpane.getVerticalScrollBar().setUnitIncrement(ROW_HEIGHT);
        upperleftcorner.setBackground(Color.WHITE);
        add(sheetscrollpane, createConstraints(0, 1.0d, GridBagConstraints.BOTH));
        add(createScrollPane(formulatextarea), createConstraints(1, 0.0d, GridBagConstraints.HORIZONTAL));
        add(statuslabel, createConstraints(2, 0.0d, GridBagConstraints.HORIZONTAL));
    }
    //
    private static Dimension calculatePreferredScrollPaneSize()
    {
        int width = MARGIN + 18 + COLUMN_WIDTH * 6;
        int height = ROW_HEIGHT * ((int) (width / GOLDEN_RATIO / ROW_HEIGHT) - 5);
        //
        return new Dimension(width, height);
    }
    //
    @Override
    public void setVisible(boolean visible)
    {
        super.setVisible(visible);
        //
        FormulaTextArea formulatextarea = mediator.getSingleton(FormulaTextArea.class);
        //
        if (visible) formulatextarea.requestFocusInWindow();
    }
    //
    private static GridBagConstraints createConstraints(int y, double weight, int fill)
    {
        return new GridBagConstraints(0, y, 1, 1, 1.0d, weight, GridBagConstraints.CENTER, fill, NO_INSETS, 0, 0);
    }
}