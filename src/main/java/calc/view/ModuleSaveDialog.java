package calc.view;
/*
 * Copyright (C) 2015, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import calc.Mediator;
import calc.model.SaveDestination;
import calc.model.ServiceModel;
import calc.model.Sheet;
import calc.model.WizardModel;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import static calc.view.Style.createScrollPane;
/**
 * Created by andreasm on 10.06.15
 */
public final class ModuleSaveDialog extends AbstractWizardDialog
{
    public static final String LOGIN_STEP = "login";
    public static final String DONE_STEP = "done";
    //
    private final JTextField namefield;
    private final JTextArea commentdearea;
    private final JTextArea exportsarea;
    private final JComboBox<String> destinationcombo;
    private final JTextField serverfield;
    private final JTextField emailaddressfield;
    private final JTextField passwordfield;
    private final JTextPane modulepane;
    //
    public ModuleSaveDialog(Mediator mediator, WizardModel model)
    {
        super(mediator, model);
        //
        Sheet sheet = mediator.getSingleton(Sheet.class);
        ServiceModel servicemodel = mediator.getSingleton(ServiceModel.class);
        //
        namefield = createTextField(sheet.getModuleName());
        commentdearea = createPlainTextArea(sheet.getModuleCommentDe());
        exportsarea = createMonospacedTextArea(String.join(" ", sheet.getModuleExports()));
        destinationcombo = createComboBox("LOCAL", "CLOUD");
        serverfield = createTextField(servicemodel.getServer());
        emailaddressfield = createTextField(servicemodel.getEmailAddress());
        passwordfield = createPasswordField(servicemodel.getPassword());
        modulepane = createEmptyTextPane();
        //
        if (servicemodel.getEmailAddress() != null && servicemodel.getPassword().length > 0) destinationcombo.setSelectedIndex(1);
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        super.postConstruct();
        //
        Box start = Box.createVerticalBox();
        start.add(createTextPane("SAVE_MODULE_DETAILS"));
        start.add(Box.createVerticalStrut(Style.GAP));
        start.add(createLabel("NAME"));
        start.add(namefield);
        start.add(Box.createVerticalStrut(Style.SMALL_GAP));
        start.add(createLabel("DESCRIPTION"));
        start.add(createScrollPane(commentdearea));
        start.add(Box.createVerticalStrut(Style.SMALL_GAP));
        start.add(createLabel("MODULE_EXPORTS"));
        start.add(createScrollPane(exportsarea));
        start.add(Box.createVerticalStrut(Style.SMALL_GAP));
        start.add(createLabel("DESTINATION"));
        start.add(destinationcombo);
        start.setBorder(new EmptyBorder(new Insets(Style.GAP, Style.GAP, 0, Style.GAP)));
        addStep(start, WizardModel.START_STEP);
        //
        Box login = Box.createVerticalBox();
        login.add(createTextPane("LOGIN_DETAILS"));
        login.add(Box.createVerticalStrut(Style.GAP));
        login.add(createLabel("SERVER_URL"));
        login.add(serverfield);
        login.add(Box.createVerticalStrut(Style.SMALL_GAP));
        login.add(createLabel("EMAIL_ADDRESS"));
        login.add(emailaddressfield);
        login.add(Box.createVerticalStrut(Style.SMALL_GAP));
        login.add(createLabel("PASSWORD"));
        login.add(passwordfield);
        login.add(Box.createGlue());
        login.setBorder(new EmptyBorder(new Insets(Style.GAP, Style.GAP, 0, Style.GAP)));
        addStep(login, LOGIN_STEP);
        //
        Box done = Box.createVerticalBox();
        done.add(modulepane);
        done.add(Box.createGlue());
        done.setBorder(new EmptyBorder(new Insets(Style.GAP, Style.GAP, 0, Style.GAP)));
        addStep(done, DONE_STEP);
        //
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        setTitle(bundle.getString("SAVE_MODULE"));
        setResizable(false);
        pack();
        setLocationRelativeTo(getOwner());
    }
    //
    public String getName()
    {
        return namefield.getText();
    }
    //
    public String getCommentDe()
    {
        return commentdearea.getText();
    }
    //
    public Collection<String> getExports()
    {
        StringTokenizer tokenizer = new StringTokenizer(exportsarea.getText(), " ", false);
        LinkedList<String> collection = new LinkedList<>();
        //
        while (tokenizer.hasMoreTokens())
        {
            collection.addLast(tokenizer.nextToken());
        }
        //
        return collection;
    }
    //
    public SaveDestination getSaveDestination()
    {
        switch (destinationcombo.getSelectedIndex())
        {
            case 0: return SaveDestination.LOCAL;
            case 1: return SaveDestination.CLOUD;
        }
        //
        assert false;
        //
        return null;
    }
    //
    public String getServer()
    {
        return serverfield.getText();
    }
    //
    public String getEmailAddress()
    {
        return emailaddressfield.getText();
    }
    //
    public char[] getPassword()
    {
        return passwordfield.getText().toCharArray();
    }
    //
    public void setModuleURI(String uri)
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        String format = bundle.getString("MODULE_DETAILS");
        modulepane.setText(String.format(format, uri));
    }
    //
    public void markNameError(boolean error)
    {
        markError(namefield, error);
    }
    //
    public void markCommentDeError(boolean error)
    {
        markError(commentdearea, error);
    }
    //
    public void markExportsError(boolean error)
    {
        markError(exportsarea, error);
    }
    //
    public void markServerError(boolean error)
    {
        markError(serverfield, error);
    }
    //
    public void markEmailAddressError(boolean error)
    {
        markError(emailaddressfield, error);
    }
    //
    public void markPasswordError(boolean error)
    {
        markError(passwordfield, error);
    }
}