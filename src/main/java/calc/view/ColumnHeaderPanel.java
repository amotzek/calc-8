package calc.view;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.SystemColor;
import javax.swing.JPanel;
import calc.Mediator;
import calc.model.Cell;
import calc.model.CellSelectionListener;
import calc.model.Column;
import calc.model.Selection;
import calc.model.Sheet;
import static calc.view.Style.*;
/*
 * Created by  andreasm 19.05.12 20:58
 */
public final class ColumnHeaderPanel extends JPanel implements CellSelectionListener
{
    private static final TextPainter centerpainter = new TextPainter(TextPainter.CENTER);
    //
    private final Mediator mediator;
    //
    public ColumnHeaderPanel(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        setForeground(Color.BLACK);
        setFont(getBoldFont());
        setDoubleBuffered(true);
        Selection selection = mediator.getSingleton(Selection.class);
        selection.addCellSelectionListener(this);
    }
    //
    public void cellSelected(Cell cell, boolean toporleft)
    {
        repaint(REPAINT_DELAY);
    }
    //
    public void cellUnselected(Cell cell)
    {
        repaint(REPAINT_DELAY);
    }
    //
    public void cellRangeChanged(Cell cell1, Cell cell2)
    {
        repaint(REPAINT_DELAY);
    }
    //
    @Override
    public Dimension getPreferredSize()
    {
        SheetPanel sheetpanel = mediator.getSingleton(SheetPanel.class);
        Dimension preferredsize = sheetpanel.getPreferredSize();
        preferredsize.height = MARGIN;
        //
        return preferredsize;
    }
    //
    @Override
    protected void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Selection selection = mediator.getSingleton(Selection.class);
        Dimension dimension = getSize();
        Rectangle rectangle = new Rectangle(0, 0, 0, dimension.height);
        Column[] columns = sheet.getColumns();
        //
        for (Column column : columns)
        {
            if (column == null) continue;
            //
            rectangle.width = column.getWidth();
            //
            if (selection.isColumnSelected(column))
            {
                graphics.setColor(HEADER_SELECTION_COLOR);
                graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                graphics.setColor(getForeground());
            }
            //
            String label = column.getLabel();
            centerpainter.drawStringIn(graphics, rectangle, label);
            rectangle.x += rectangle.width;
        }
        //
        graphics.setColor(SystemColor.controlShadow);
        graphics.drawLine(0, dimension.height - 1, dimension.width - 1, dimension.height - 1);
        graphics.setColor(getForeground());
    }
}