package calc.view;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Color;
import java.awt.Dimension;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import calc.Mediator;
import calc.model.Column;
import calc.model.Log;
import calc.model.LogEntry;
import calc.model.LogListener;
import calc.model.Row;
import lisp.Chars;
import lisp.Sexpression;
import lisp.Symbol;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by  andreasm 20.01.13 11:00
 */
public final class StatusLabel extends JLabel implements LogListener
{
    private final Mediator mediator;
    //
    public StatusLabel(Mediator mediator)
    {
        this.mediator = mediator;
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        setForeground(Color.BLACK);
        setFont(getBoldFont());
        setOpaque(true);
        Log log = mediator.getSingleton(Log.class);
        log.addLogListener(this);
    }
    //
    public void logChanged()
    {
        Log log = mediator.getSingleton(Log.class);
        LogEntry entry = log.getLogEntry();
        //
        if (entry != null)
        {
            final String message = getMessage(entry);
            //
            invokeLater(() -> {
                if (message.length() > TOOLTIP_LIMIT)
                {
                    setText(message.substring(0, TOOLTIP_LIMIT) + " ...");
                    setToolTipText(message);
                }
                else
                {
                    setText(message);
                    setToolTipText(null);
                }
            });
            //
            return;
        }
        //
        invokeLater(() -> {
            setText("");
            setToolTipText(null);
        });
    }
    //
    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(300, ROW_HEIGHT);
    }
    //
    private String getMessage(LogEntry entry)
    {
        ResourceBundle bundle = mediator.getSingleton(ResourceBundle.class);
        Symbol symbol = entry.getSymbol();
        Sexpression sexpression = entry.getSexpression();
        Row row = entry.getRow();
        Column column = entry.getColumn();
        StringBuilder builder = new StringBuilder();
        //
        if (row == null || column == null)
        {
            builder.append(bundle.getString("PROGRAM"));
        }
        else
        {
            String coordinates = column.toString() + row.toString();
            String cellcoordinates = MessageFormat.format(bundle.getString("CELL_RC"), coordinates);
            builder.append(cellcoordinates);
        }
        //
        builder.append(": ");
        //
        if (symbol != null)
        {
            builder.append(symbol);
            builder.append(" ");
        }
        //
        if (sexpression instanceof Chars)
        {
            Chars chars = (Chars) sexpression;
            builder.append(chars.getString());
        }
        else
        {
            builder.append(sexpression);
        }
        //
        return builder.toString();
    }
}