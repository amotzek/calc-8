package calc.view;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Calc App package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.TransferHandler;
import calc.Mediator;
import calc.controller.transfer.SheetPanelTransferHandler;
import calc.model.Calculation;
import calc.model.CalculationListener;
import calc.model.Cell;
import calc.model.CellLockListener;
import calc.model.CellSelectionListener;
import calc.model.CellValueListener;
import calc.model.Column;
import calc.model.Row;
import calc.model.Selection;
import calc.model.Sheet;
import static calc.view.Style.*;
import static javax.swing.SwingUtilities.invokeLater;
/*
 * Created by  andreasm 19.05.12 20:30
 */
public final class SheetPanel extends JPanel implements CellValueListener, CellLockListener, CellSelectionListener, CalculationListener
{
    private static final TextPainter centerpainter = new TextPainter(TextPainter.CENTER);
    //
    private final Mediator mediator;
    //
    public SheetPanel(Mediator mediator)
    {
        super();
        //
        this.mediator = mediator;
    }
    //
    @SuppressWarnings("unused")
    public void postConstruct()
    {
        setBackground(Color.WHITE);
        setForeground(Color.BLACK);
        setFont(getPlainFont());
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Calculation calculation = mediator.getSingleton(Calculation.class);
        Selection selection = mediator.getSingleton(Selection.class);
        sheet.addCellValueListener(this);
        sheet.addCellLockListener(this);
        calculation.addCalculationListener(this);
        selection.addCellSelectionListener(this);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int keymask = toolkit.getMenuShortcutKeyMask();
        KeyStroke ctrlx = KeyStroke.getKeyStroke(KeyEvent.VK_X, keymask);
        KeyStroke ctrlc = KeyStroke.getKeyStroke(KeyEvent.VK_C, keymask);
        KeyStroke ctrlv = KeyStroke.getKeyStroke(KeyEvent.VK_V, keymask);
        InputMap inputmap = getInputMap();
        ActionMap actionmap = getActionMap();
        setTransferHandler(new SheetPanelTransferHandler(mediator));
        putAction(inputmap, actionmap, ctrlx, TransferHandler.getCutAction());
        putAction(inputmap, actionmap, ctrlc, TransferHandler.getCopyAction());
        putAction(inputmap, actionmap, ctrlv, TransferHandler.getPasteAction());
    }
    //
    private static void putAction(InputMap inputmap, ActionMap actionmap, KeyStroke keystroke, Action action)
    {
        Object name = action.getValue(Action.NAME);
        inputmap.put(keystroke, name);
        actionmap.put(name, action);
    }
    //
    public Cell findCell(Point point)
    {
        Column column = findColumn(point.x);
        Row row = findRow(point.y);
        //
        if (column == null || row == null) return null;
        //
        Sheet sheet = mediator.getSingleton(Sheet.class);
        //
        return sheet.findCell(column, row);
    }
    //
    public void cellValueChanged(Cell cell)
    {
        repaintCell(cell);
    }
    //
    public void cellLockChanged(Cell cell)
    {
        repaintCell(cell);
    }
    //
    public void cellSelected(Cell cell, boolean toporleft)
    {
        repaintColumnAndRow(cell);
    }
    //
    public void cellUnselected(Cell cell)
    {
        repaintColumnAndRow(cell);
    }
    //
    public void cellRangeChanged(Cell cell1, Cell cell2)
    {
        repaintAll();
    }
    //
    public void calculationStateChanged()
    {
        Calculation calculation = mediator.getSingleton(Calculation.class);
        final Cursor cursor = (calculation.isRunning()) ? getWaitCursor() : null;
        //
        invokeLater(() -> SheetPanel.this.setCursor(cursor));
    }
    //
    @Override
    protected void paintComponent(Graphics graphics)
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Selection selection = mediator.getSingleton(Selection.class);
        super.paintComponent(graphics);
        Rectangle clipbounds = graphics.getClipBounds();
        Rectangle cellbounds = new Rectangle();
        Row[] rows = sheet.getRows();
        Column[] columns = sheet.getColumns();
        //
        for (Row row : rows)
        {
            if (row == null) continue;
            //
            cellbounds.x = 0;
            cellbounds.height = row.getHeight();
            //
            for (Column column : columns)
            {
                if (column == null) continue;
                //
                cellbounds.width = column.getWidth();
                //
                if (clipbounds.intersects(cellbounds))
                {
                    Cell cell = sheet.findCell(column, row);
                    boolean selectedrow = selection.isRowSelected(row);
                    boolean selectedcolumn = selection.isColumnSelected(column);
                    //
                    if (selectedrow && selectedcolumn)
                    {
                        graphics.setColor(CELL_SELECTION_COLOR);
                        graphics.fillRect(cellbounds.x, cellbounds.y, cellbounds.width, cellbounds.height);
                        graphics.setColor(Color.WHITE);
                    }
                    else if (selectedrow || selectedcolumn)
                    {
                        graphics.setColor(RULER_SELECTION_COLOR);
                        graphics.fillRect(cellbounds.x, cellbounds.y, cellbounds.width, cellbounds.height);
                        graphics.setColor(getForeground());
                    }
                    else if (cell.isLocked())
                    {
                        graphics.setColor(LOCK_COLOR);
                        graphics.fillRect(cellbounds.x, cellbounds.y, cellbounds.width, cellbounds.height);
                        graphics.setColor(getForeground());
                    }
                    //
                    if (cell.hasValue())
                    {
                        String string = cell.getValueString();
                        centerpainter.drawStringIn(graphics, cellbounds, string);
                    }
                    //
                    graphics.setColor(getForeground());
                }
                //
                cellbounds.x += cellbounds.width;
            }
            //
            cellbounds.y += cellbounds.height;
        }
    }
    //
    @Override
    public Dimension getMinimumSize()
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Column[] columns = sheet.getColumns();
        Row[] rows = sheet.getRows();
        int w = 0;
        int h = 0;
        //
        for (Column column : columns)
        {
            if (column == null) continue;
            //
            w += column.getWidth();
        }
        //
        for (Row row : rows)
        {
            if (row == null) continue;
            //
            h += row.getHeight();
        }
        //
        return new Dimension(w, h);
    }
    //
    @Override
    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }
    //
    private void repaintColumnAndRow(Cell cell)
    {
        Dimension size = getSize();
        Rectangle bounds = getBounds(cell);
        repaint(REPAINT_DELAY, bounds.x, 0, bounds.width, size.height);
        repaint(REPAINT_DELAY, 0, bounds.y, size.width, bounds.height);
    }
    //
    private void repaintCell(Cell cell)
    {
        Rectangle rectangle = getBounds(cell);
        repaint(REPAINT_DELAY, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }
    //
    private void repaintAll()
    {
        Dimension size = getSize();
        repaint(REPAINT_DELAY, 0, 0, size.width, size.height);
    }
    //
    private Rectangle getBounds(Cell cell)
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Column column = cell.getColumn();
        Row row = cell.getRow();
        Column[] columns = sheet.getColumns();
        Row[] rows = sheet.getRows();
        int x = 0;
        int y = 0;
        //
        for (int i = 1; i < column.getIndex(); i++)
        {
            x += columns[i].getWidth();
        }
        //
        for (int i = 1; i < row.getIndex(); i++)
        {
            y += rows[i].getHeight();
        }
        //
        int w = column.getWidth();
        int h = row.getHeight();
        //
        return new Rectangle(x, y, w, h);
    }
    //
    private Column findColumn(int x)
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Column[] columns = sheet.getColumns();
        //
        for (Column column : columns)
        {
            if (column == null) continue;
            //
            x -= column.getWidth();
            //
            if (x <= 0) return column;
        }
        //
        return null;
    }
    //
    private Row findRow(int y)
    {
        Sheet sheet = mediator.getSingleton(Sheet.class);
        Row[] rows = sheet.getRows();
        //
        for (Row row : rows)
        {
            if (row == null) continue;
            //
            y -= row.getHeight();
            //
            if (y <= 0) return row;
        }
        //
        return null;
    }
}